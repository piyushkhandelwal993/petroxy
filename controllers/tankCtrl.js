var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    showiTankPage: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var QueryString;
        var query;
        var ttype=this.request.query.ttype;
        var ttype;
        if((!ttype) || ttype=='All'){
            QueryString='select * from tank_metadata where pid=%s and del=0';
            query=util.format(QueryString,pid);
        }
        else{
            QueryString='select * from tank_metadata where pid=%s and type="%s" and del=0';
            query=util.format(QueryString,pid,ttype);
        }
        
        var DetailtankResult=yield databaseUtils.executeQuery(query);

        var QueryString='select distinct(type) from tank_metadata where pid=%s and del=0';
        var query=util.format(QueryString,pid);
        var tankType=yield databaseUtils.executeQuery(query);

        console.log(this.currentUser);

        yield this.render('itank',{
            DetailtankResult:DetailtankResult,
            tankType:tankType,
            ttype:ttype,
    });
},
showiTank2Page: function* (next) {
    var pid;
    var tid=this.request.body.tid;
    var cap=this.request.body.capacity;
    var type=this.request.body.type;
    if(tid==undefined||cap==undefined||type==undefined){}
    else{
    var queryString='update tank_metadata set capacity=%s,type="%s" where id=%s';
    var query=util.format(queryString,cap,type,tid);
    var res=yield databaseUtils.executeQuery(query);
    }

    try{
    var pid=this.request.body.pid;
    var cap=this.request.body.capacity;
    var type=this.request.body.type;
    var queryString='insert into tank_metadata (pid,capacity,type) \
    values(%s,%s,"%s")';
    var query=util.format(queryString,pid,cap,type);
    var res=yield databaseUtils.executeQuery(query);
    }
    catch(e){
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

    }
    var DetailtankQueryString;
    var DetailtankQuery;
    var wf;
    try{
        var wf=this.request.body.ttype;
        if (wf=='all'){
            DetailtankQueryString='select id,capacity,type,active from tank_metadata where pid=%s';
            DetailtankQuery=util.format(DetailtankQueryString,pid);
        }
        else{
            DetailtankQueryString='select id,capacity,type,active from tank_metadata where pid=%s and type="%s"';
            DetailtankQuery=util.format(DetailtankQueryString,pid,wf);
        }
    }
    catch(ee){
    }
    if(wf==undefined){
    DetailtankQueryString='select id,capacity,type,active from tank_metadata where pid=%s';
    DetailtankQuery=util.format(DetailtankQueryString,pid);
    }
    
    var DetailtankResult=yield databaseUtils.executeQuery(DetailtankQuery);
    
    

    var tankTypeQueryString='select distinct(type) from tank_metadata where pid=%s';
    var tankTypeQuery=util.format(tankTypeQueryString,pid);
    var tankType=yield databaseUtils.executeQuery(tankTypeQuery);

    this.redirect('itank');
},
showiTank3Page: function* (next) {
    var pid;
    try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

    var queryString;
    var query;
    var tid;
    var cap=this.request.body.capacity;
    var type=this.request.body.type;
    var qty=this.request.body.qty;
    var price=this.request.body.price;
    var tno=this.request.body.tno;
    console.log(this.request.body.tid);
    var act=this.request.body.act.split(' ');

    if(parseInt(act[0])==1){
        var tcount=yield databaseUtils.executeQuery(util.format('select * from tank_metadata where pid=%s',pid));
        var queryString='insert into tank_metadata (pid,capacity,type,qty,price,tank_no) \
        values(%s,%s,"%s",%s,%s,%s)';
        var query=util.format(queryString,pid,cap,type,qty,price,tcount.length+1);
        var res=yield databaseUtils.executeQuery(query);
    }
    else if(parseInt(act[0])==2){
        var tid=parseInt(act[1]);
        var active=this.request.body.active;
        console.log(act);
        if(price && qty){
            var queryString='update tank_metadata set qty=%s,price=%s,active=1 where id=%s';
            var query=util.format(queryString,qty,price,tid);
            var res=yield databaseUtils.executeQuery(query);
        }
        else{
            var re=yield databaseUtils.executeQuery(util.format('update tank_metadata set active=%s where id=%s',active,tid));
        }

    }
    else{
        var tid=parseInt(act[2]);
        var res=yield databaseUtils.executeQuery(util.format('update tank_metadata set del=1 where id=%s',tid));
        
    }
    this.redirect('itank');
},
}


