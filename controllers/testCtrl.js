
var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');
var state = require('./../controllers/state');
module.exports={

    showdistrict: function*(next){
        var s=state.state;
        var arr=[];
        for(i in s) arr.push(i);
        arr.sort();
        this.body=arr;
    },
    getdistricts: function*(next){
        var s=state.state;
        var ss=this.request.query.state;
        this.body=s[ss];
    },
    getmodes: function*(next){
        var pid=this.currentUser.pid;
        var modes=yield databaseUtils.executeQuery(util.format('select * from collection_mode where pid=%s and mode in ("credit","bank") order by mode',pid));
        var arr=[];
        a=modes[0].id;
        b=modes[1].id;
        arr.push(a);arr.push(b);
        this.body=arr;
    },
    getcreditparty: function*(next){
        var pid=this.currentUser.pid;
        var id=this.request.body.id;
        console.log(id);
        if(id){

            var parties=yield databaseUtils.executeQuery(util.format('select * from credit where pid=%s and id=%s',pid,id));
            var arr=[];
            for(i in parties) arr.push(parties[i]);
            this.body=arr;

        }else{
            var parties=yield databaseUtils.executeQuery(util.format('select * from credit where pid=%s',pid));
        var arr=[];
        for(i in parties) arr.push(parties[i]);
        this.body=arr;
        }
        
    },
    getbankaccount: function*(next){
        var pid=this.currentUser.pid;
        var bank=this.request.body.bank;
        if(bank){
        var bankaccounts=yield databaseUtils.executeQuery(util.format('select * from bank_account where pid=%s and bank="%s"',pid,bank));
        var arr=[];
        for(i in bankaccounts) arr.push(bankaccounts[i]);
        this.body=arr;

        }else{
            var bankaccounts=yield databaseUtils.executeQuery(util.format('select * from bank_account where pid=%s',pid));
            var arr=[];
            for(i in bankaccounts) arr.push(bankaccounts[i]);
            this.body=arr;
        }
        console.log(arr);
    },
    getproducts: function*(next){
        var pid=this.currentUser.pid;
           var  item=yield databaseUtils.executeQuery(util.format('select * from product where pid=%s',pid));
           console.log(item);
        var arr=[];
        for(i in item) arr.push(item[i]);
        this.body=arr;
    },
    getdsms:function*(next){
        var pid=this.currentUser.pid;
        
        dsmUsers= yield databaseUtils.executeQuery(util.format('select distinct(fname),u.id,lname from shift_nozzle_dsm snd left join user u on u.id=snd.user_id where snd.shift_id=(select shift_id from shift_nozzle_dsm where pid=%s and dcr=0 limit 1)',pid));
       console.log(dsmUsers);
        var arr=[];
        for(i in dsmUsers) arr.push(dsmUsers[i]);
        this.body=arr;
    },
    getfuels: function*(next){
        var pid=this.currentUser.pid;
        var fuels=yield databaseUtils.executeQuery(util.format('select id,type from tank_metadata where pid=%s',pid));
        var arr=[];
        for(i in fuels) arr.push(fuels[i]);
        this.body=arr;

    },
    gettransports: function*(next){
        var pid=this.currentUser.pid;
        var transports=yield databaseUtils.executeQuery(util.format('select * from transport where pid=%s',pid));
        var arr=[];
        for(i in transports) arr.push(transports[i]);
        this.body=arr;
    },
    getfuelprice: function*(next){
        var pid=this.currentUser.pid;
        var prices=yield databaseUtils.executeQuery(util.format('select * from tank_metadata where pid=%s',pid));
        var arr=[];
        for(i in prices) arr.push(prices[i]);
        this.body=arr;
    },
    setfuelprices: function*(next){
        var arr=this.request.body.price;
        var pid=this.currentUser.pid;
        for(i in arr)
        yield databaseUtils.executeQuery(util.format('update tank_metadata set price=%s where type="%s" and pid=%s',parseFloat(arr[i].price),arr[i].id.toString(),pid));
        flag={msg:'Price Changed SuccessfullY !!!'}
        var arr=[];arr.push(flag);
        console.log(arr);
        this.body=flag;
    },
    checkpmail:function*(next){
        var mail=this.request.body.email;
        var r=yield databaseUtils.executeQuery(util.format('select * from petrolpump where email="%s"',mail));
        if(r.length==0) this.body={flag:'1'}
        else this.body={flag:'0'}
    },
    
    checkpphone:function*(next){
        var phone=this.request.body.phone;
        var r=yield databaseUtils.executeQuery(util.format('select * from petrolpump where phone="%s"',phone));
        console.log(phone);
        if(r.length==0) this.body={flag:'1'}
        else this.body={flag:'0'}
    },
    
    checkaemail:function*(next){
        var email=this.request.body.email;
        console.log(email);
        var r=yield databaseUtils.executeQuery(util.format('select * from user where email="%s"',email));
        if(r.length==0) this.body={flag:'1'}
        else this.body={flag:'0'}
    },
    
    checkadhaar:function*(next){
        var adhaar=this.request.body.adhaar;
        console.log(adhaar);
        var r=yield databaseUtils.executeQuery(util.format('select * from user where adhaar="%s"',adhaar));
        if(r.length==0) this.body={flag:'1'}
        else this.body={flag:'0'}
    },
    checkaphone:function*(next){
        var phone=this.request.body.phone;
        console.log(phone);
        var r=yield databaseUtils.executeQuery(util.format('select * from user where phone="%s" ',phone));
        if(r.length==0) this.body={flag:'1'}
        else this.body={flag:'0'}
    },
    getnooftanks:function*(next){
        var pid=this.currentUser.pid;
        var res=yield databaseUtils.executeQuery(util.format('select * from tank_metadata where pid=%s',pid));
        this.body={value:(res.length+1)}
    },
    getproductprice: function*(next){
       //s var pid=this.currentUser.pid;
        var product=this.request.body.product;
        var re=yield databaseUtils.executeQuery(util.format('select * from product where id=%s',product));
        console.log('api chali');
        this.body={price:re[0].price}

    },
    getmodes1: function*(next){
        var pid=this.currentUser.pid;
        var modes=yield databaseUtils.executeQuery(util.format('select * from collection_mode where pid=%s order by mode',pid));
        var arr=[];
        for(i in modes) arr.push(modes[i]);
        this.body=arr;
    },

}


