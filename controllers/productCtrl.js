var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    showiNozzlePage: function* (next) {
        var pid;
        try{ pid=this.currentUser.pid;}
        catch(e){pid=0;}

        var ntype=this.request.query.ntype;
       // console.log(pid,ntype);

        var queryString;
        var query;

        if(ntype && ntype!='All'){
            queryString='select * from nozzle where pid="%s" and type="%s" and del=0';
            query=util.format(queryString,pid,ntype);
        }
        else{
            queryString='select * from nozzle where pid="%s"  and del=0';
            query=util.format(queryString,pid);
        }

            var nozzleDetails=yield databaseUtils.executeQuery(query);

            queryString='select distinct(type) from nozzle where pid=%s';
            query=util.format(queryString,pid);
            var nozzleType=yield databaseUtils.executeQuery(query);

            queryString='select type from tank_metadata where pid=%s';
            query=util.format(queryString,pid);
            var tankTypes=yield databaseUtils.executeQuery(query);

            yield this.render('inozzle',{
                nozzleDetails:nozzleDetails,
                nozzleType:nozzleType,
                ntype:ntype,
                tankTypes:tankTypes,
        });
    },
    showiNozzle2Page: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var queryString;
        var query;
        var nozzleDetails;
        
        var act=this.request.body.act.split(' ');
        var nno=this.request.body.nno;
        var ntype=this.request.body.ntype;
            if(parseInt(act[0])==1){
                    queryString='insert into nozzle (pid,nozzle_number,type) \
                    values(%s,%s,"%s")';
                    query=util.format(queryString,pid,nno,ntype);
                    var res=yield databaseUtils.executeQuery(query);
            }
            else if(parseInt(act[0])==2) {
                queryString='update nozzle set active=%s where id=%s';
                var active=this.request.body.active;
                console.log(active,parseInt(act[1]));
                query=util.format(queryString,active,parseInt(act[1]));
                
                var r=yield databaseUtils.executeQuery(query);

            }
            else{
                console.log('ye waala console',act);

                    var res=yield databaseUtils.executeQuery(util.format('update nozzle set del=1 where id=%s',parseInt(act[2])));
            }

            this.redirect('inozzle');
},
    showiTankPage: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var QueryString;
        var query;
        var ttype=this.request.query.ttype;
        var ttype;
        if((!ttype) || ttype=='All'){
            QueryString='select id,capacity,type,active from tank_metadata where pid=%s';
            query=util.format(QueryString,pid);
        }
        else{
            QueryString='select id,capacity,type,active from tank_metadata where pid=%s and type="%s"';
            query=util.format(QueryString,pid,ttype);
        }
        
        var DetailtankResult=yield databaseUtils.executeQuery(query);

        var QueryString='select distinct(type) from tank_metadata where pid=%s';
        var query=util.format(QueryString,pid);
        var tankType=yield databaseUtils.executeQuery(query);
        yield this.render('itank',{
            DetailtankResult:DetailtankResult,
            tankType:tankType,
            ttype:ttype,
    });
},

}

var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    showiProductPage: function* (next) {
        var pid;
        try{ pid=this.currentUser.pid;}
        catch(e){pid=0;}
        var ptype=this.request.query.ptype;
        var pname=this.request.query.pname;
        console.log(pid,pname,ptype);
        var queryString;
        var query;
        if(pname && pname!='All'){
            queryString='select * from product where pid="%s" and name="%s" and del=0';
            query=util.format(queryString,pid,pname);
        }
        else if(ptype && ptype!='All'){
            queryString='select * from product where pid="%s" and type=%s and del=0';
            query=util.format(queryString,pid,parseInt(ptype));
        }
        else{
            queryString='select * from product where pid="%s" and del=0';
            query=util.format(queryString,pid);
        }

            var productDetails=yield databaseUtils.executeQuery(query);

            queryString='select name from product where pid=%s and del=0';
            query=util.format(queryString,pid);
            var productname=yield databaseUtils.executeQuery(query);

            yield this.render('iproduct',{
                productDetails:productDetails,
                productname:productname,
                pname:pname,
                ptype:ptype,
        });
    },
    showiProduct2Page: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var queryString;
        var query;
        var productDetails
        var ppid=this.request.body.ppid;
        var pname=this.request.body.name;
        var ptype=0;
        var price=this.request.body.price;
        var qty=this.request.body.qty;
        var act=this.request.body.act.split(' ');


        if(parseInt(act[0])==1){
                   
                    queryString='insert into product (pid,name,price,qty) \
                    values(%s,"%s",%s,%s)';
                    query=util.format(queryString,pid,pname,price,qty);
                    var res=yield databaseUtils.executeQuery(query);
                    queryString='SELECT * from product where pid="%s"';
                    query=util.format(queryString,pid);
                     productDetails=yield databaseUtils.executeQuery(query);
        }
       
        else if(parseInt(act[0])==2) {
            var ppid=parseInt(act[1]);
            var active=this.request.body.active;
            console.log(pname,price,qty,ppid);
            if(price){
                queryString='update product set name="%s",price=%s,qty=%s where id=%s';
                query=util.format(queryString,pname,price,qty,ppid);
                var r=yield databaseUtils.executeQuery(query);
            }
            else{
                var re=yield databaseUtils.executeQuery(util.format('update product set active=%s where id=%s',active,ppid));
            }

        }
      

        else {
            var ppid=parseInt(act[2]);
            var res=yield databaseUtils.executeQuery(util.format('update product set del=1 where id=%s',ppid));
        }

            

            this.redirect('iproduct');
},
    showiTankPage: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var QueryString;
        var query;
        var ttype=this.request.query.ttype;
        var ttype;
        if((!ttype) || ttype=='All'){
            QueryString='select id,capacity,type,active from tank_metadata where pid=%s';
            query=util.format(QueryString,pid);
        }
        else{
            QueryString='select id,capacity,type,active from tank_metadata where pid=%s and type="%s"';
            query=util.format(QueryString,pid,ttype);
        }
        
        var DetailtankResult=yield databaseUtils.executeQuery(query);

        var QueryString='select distinct(type) from tank_metadata where pid=%s';
        var query=util.format(QueryString,pid);
        var tankType=yield databaseUtils.executeQuery(query);
        yield this.render('itank',{
            DetailtankResult:DetailtankResult,
            tankType:tankType,
            ttype:ttype,
    });
},

}


