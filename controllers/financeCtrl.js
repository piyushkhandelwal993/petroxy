var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    
    showfinancePage: function* (next) {

        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}
        var nozzle_number=this.request.query.no;
        if (nozzle_number==undefined) {
            nozzle_number=1;
        }

        var queryString;
        var query;
        
        //querry to show all details about all shifts
        queryString='select u1.fname as supervisor,u2.fname as assigned_to,type,nozzle_number,shift.start_time,snd.user_id,(dor-dcr-pump_test-self) as sale,sc.id,mode,amount from shift,shift_nozzle_dsm snd,user u1,nozzle,shift_collection sc,collection_mode cm, user u2 where shift.id=snd.shift_id and u1.id=shift.supervisor_id and snd.nozzle_id=nozzle.id and snd.id=sc.shift_nozzle_dsm_id and snd.user_id=u2.id and cm.id=sc.collection_mode_id and shift.pid=%s and shift.end_time>=\'2018-01-01 00:00:00\' and shift.end_time<=\'2018-06-25 18:00:00\'';
        query=util.format(queryString,pid);
        var AllDetailfinanceResult=yield databaseUtils.executeQuery(query);
        var shiftDetails=AllDetailfinanceResult;

       //query to show sales according to ALL nozzle number
       queryString='select user.fname as fname,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from shift,shift_nozzle_dsm,user,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and user.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift.pid=%s and shift.start_time>=\'2018-01-01 00:00:00\' and shift.start_time<=\'2018-08-08 18:00:00\' group by nozzle_number,mode';
       query=util.format(queryString,pid);
       var SalesNozzleResult=yield databaseUtils.executeQuery(query);
       var saleDetails=SalesNozzleResult;

       
       //QUERY TO SHOW SALES ACCORDING TO PARTICULAR NOZZLE NUMBER
        
       queryString='select user.fname as fname,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from shift,shift_nozzle_dsm,user,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and user.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift.pid=%s and nozzle_number=%s and shift.start_time>=\'2018-01-01 00:00:00\' and shift.start_time<=\'2018-08-08 18:00:00\'';
       query=util.format(queryString,pid,nozzle_number);
       var SalesNozzleResult1=yield databaseUtils.executeQuery(query);
       var saleDetails1=SalesNozzleResult1;
       
       //show details of a specific supervisor

       
       queryString='select user.fname as fname,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from shift,shift_nozzle_dsm,user,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and user.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift.pid=%s and shift.start_time>=\'2018-01-01 00:00:00\' and shift.start_time<=\'2018-08-08 18:00:00\' and user.fname=\'aman\' group by nozzle_number,mode';
       query=util.format(queryString,pid);
       var SalesSupervisorResult=yield databaseUtils.executeQuery(query);
       var supervisorDetails=SalesSupervisorResult;
       // SHOW DETAILS OF ALL SUPERVISOR

       queryString='select user.fname as fname,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from shift,shift_nozzle_dsm,user,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and user.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift.pid=%s and shift.start_time>=\'2018-01-01 00:00:00\' and shift.start_time<=\'2018-08-08 18:00:00\' group by user.fname, nozzle_number,mode';
       query=util.format(queryString,pid);
       var SalesSupervisorResult1=yield databaseUtils.executeQuery(query);
       var supervisorDetails1=SalesSupervisorResult1;

       //to show details according to particular dsm
        
       queryString='select u1.fname as supervisor,u2.fname as dsm,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from user u1,shift,shift_nozzle_dsm,user u2,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and u1.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift_nozzle_dsm.user_id=u2.id and shift.pid=%s and shift.start_time>=\'2018-01-01 00:00:00\' and shift.start_time<=\'2018-08-08 18:00:00\' and u2.fname=\'sita\'';
       query=util.format(queryString,pid);
       var SalesDSMResult=yield databaseUtils.executeQuery(query);
        var dsmDetails=SalesDSMResult;
       // TO SHOW DETAILS OF ALL DSM

       queryString='select u1.fname as supervisor,u2.fname as dsm,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from user u1,shift,shift_nozzle_dsm,user u2,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and u1.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift_nozzle_dsm.user_id=u2.id and shift.pid=%s and shift.start_time>=\'2018-01-01 00:00:00\' and shift.start_time<=\'2018-08-08 18:00:00\' group by nozzle_number,start_time,mode';
       query=util.format(queryString,pid);
       var SalesDSMResult1=yield databaseUtils.executeQuery(query);
        var dsmDetails1=SalesDSMResult1;

        yield this.render('finance',{
        shiftDetails:shiftDetails,
        saleDetails:saleDetails,
       saleDetails1:saleDetails1,
        supervisorDetails:supervisorDetails,
      supervisorDetails1:supervisorDetails1,
       dsmDetails:dsmDetails,
       dsmDetails1:dsmDetails1,
    //   supdsmDetails:supdsmDetails 

        });
    },
    showifinancePage: function* (next) {

        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var queryString;
        var query;
        var from;
        var to;
        var nozzle;
        var supervisor;
        var mode;
        var dsm;
        var type;
        var f1;
        var f2;
        var f3;
        var f4;
        var f5;
        try{from=this.request.query.from;}catch(e){}
        try{to=this.request.query.to;}catch(e){}
        try{f1=this.request.query.f1;}catch(e){}
        try{f2=this.request.query.f2;}catch(e){}
        try{f3=this.request.query.f3;}catch(e){}
        try{f4=this.request.query.f4;}catch(e){}
        try{f5=this.request.query.f5;}catch(e){}
        try{nozzle=this.request.query.nozzle;}catch(e){}
        try{supervisor=this.request.query.supervisor;}catch(e){}
        try{dsm=this.request.query.dsm;}catch(e){}
        try{mode=this.request.query.mode;}catch(e){}
        try{type=this.request.query.type;}catch(e){}

        console.log(f1,f2,f3,f4,f5,nozzle,supervisor,dsm,mode,type);

        /*
        
        //querry to show all details about all shifts
        queryString='SELECT U1.FNAME AS SUPERVISOR,U2.FNAME AS ASSIGNED_TO,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME,SND.USER_id,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SC.id,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM SND,USER U1,NOZZLE,SHIFT_COLLECTION SC,COLLECTION_MODE CM, USER U2 WHERE SHIFT.id=SND.SHIFT_id AND U1.id=SHIFT.SUPERVISOR_id AND SND.NOZZLE_id=NOZZLE.id AND SND.id=SC.SHIFT_NOZZLE_DSM_id AND SND.USER_id=U2.id AND CM.id=SC.COLLECTION_MODE_id AND SHIFT.pid=%s AND SHIFT.END_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.END_TIME<=\'2018-06-25 18:00:00\'';
        query=util.format(queryString,pid);
        var AllDetailfinanceResult=yield databaseUtils.executeQuery(query);
        var shiftDetails=AllDetailfinanceResult;

       //query to show sales according to ALL nozzle number
       queryString='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_id AS USER_id,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.id AS id,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.id=SHIFT_NOZZLE_DSM.SHIFT_id AND USER.id=SHIFT.SUPERVISOR_id AND SHIFT_NOZZLE_DSM.NOZZLE_id=NOZZLE.id AND SHIFT_NOZZLE_DSM.id=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_id AND COLLECTION_MODE.id=SHIFT_COLLECTION.COLLECTION_MODE_id AND SHIFT.pid=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' GROUP BY NOZZLE_NUMBER,MODE';
       query=util.format(queryString,pid);
       var SalesNozzleResult=yield databaseUtils.executeQuery(query);
       var saleDetails=SalesNozzleResult;

       
       //QUERY TO SHOW SALES ACCORDING TO PARTICULAR NOZZLE NUMBER
        
       queryString='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_id AS USER_id,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.id AS id,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.id=SHIFT_NOZZLE_DSM.SHIFT_id AND USER.id=SHIFT.SUPERVISOR_id AND SHIFT_NOZZLE_DSM.NOZZLE_id=NOZZLE.id AND SHIFT_NOZZLE_DSM.id=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_id AND COLLECTION_MODE.id=SHIFT_COLLECTION.COLLECTION_MODE_id AND SHIFT.pid=%s AND NOZZLE_NUMBER=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\'';
       query=util.format(queryString,pid,nozzle_number);
       var SalesNozzleResult1=yield databaseUtils.executeQuery(query);
       var saleDetails1=SalesNozzleResult1;
       
       //show details of a specific supervisor

       
       queryString='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_id AS USER_id,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.id AS id,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.id=SHIFT_NOZZLE_DSM.SHIFT_id AND USER.id=SHIFT.SUPERVISOR_id AND SHIFT_NOZZLE_DSM.NOZZLE_id=NOZZLE.id AND SHIFT_NOZZLE_DSM.id=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_id AND COLLECTION_MODE.id=SHIFT_COLLECTION.COLLECTION_MODE_id AND SHIFT.pid=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' AND USER.FNAME=\'AMAN\' group by nozzle_number,mode';
       query=util.format(queryString,pid);
       var SalesSupervisorResult=yield databaseUtils.executeQuery(query);
       var supervisorDetails=SalesSupervisorResult;
       // SHOW DETAILS OF ALL SUPERVISOR

       queryString='SELECT USER.FNAME AS FNAME,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_id AS USER_id,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.id AS id,MODE,AMOUNT FROM SHIFT,SHIFT_NOZZLE_DSM,USER,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.id=SHIFT_NOZZLE_DSM.SHIFT_id AND USER.id=SHIFT.SUPERVISOR_id AND SHIFT_NOZZLE_DSM.NOZZLE_id=NOZZLE.id AND SHIFT_NOZZLE_DSM.id=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_id AND COLLECTION_MODE.id=SHIFT_COLLECTION.COLLECTION_MODE_id AND SHIFT.pid=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' group by user.fname, nozzle_number,mode';
       query=util.format(queryString,pid);
       var SalesSupervisorResult1=yield databaseUtils.executeQuery(query);
       var supervisorDetails1=SalesSupervisorResult1;

       //to show details according to particular dsm
        
       queryString='SELECT U1.FNAME AS SUPERVISOR,U2.FNAME AS DSM,TYPE,NOZZLE_NUMBER,SHIFT.START_TIME AS START_TIME,SHIFT_NOZZLE_DSM.USER_id AS USER_id,(DOR-DCR-PUMP_TEST-SELF) AS SALE,SHIFT_COLLECTION.id AS id,MODE,AMOUNT FROM USER U1,SHIFT,SHIFT_NOZZLE_DSM,USER U2,NOZZLE,SHIFT_COLLECTION,COLLECTION_MODE WHERE SHIFT.id=SHIFT_NOZZLE_DSM.SHIFT_id AND U1.id=SHIFT.SUPERVISOR_id AND SHIFT_NOZZLE_DSM.NOZZLE_id=NOZZLE.id AND SHIFT_NOZZLE_DSM.id=SHIFT_COLLECTION.SHIFT_NOZZLE_DSM_id AND COLLECTION_MODE.id=SHIFT_COLLECTION.COLLECTION_MODE_id AND SHIFT_NOZZLE_DSM.USER_id=U2.id AND SHIFT.pid=%s AND SHIFT.START_TIME>=\'2018-01-01 00:00:00\' AND SHIFT.START_TIME<=\'2018-08-08 18:00:00\' AND U2.FNAME=\'SITA\'';
       query=util.format(queryString,pid);
       var SalesDSMResult=yield databaseUtils.executeQuery(query);
        var dsmDetails=SalesDSMResult;
       // TO SHOW DETAILS OF ALL DSM
        */
        if(from){
            queryString='select u1.fname as supervisor,u2.fname as dsm,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from user u1,shift,shift_nozzle_dsm,user u2,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and u1.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift_nozzle_dsm.user_id=u2.id and shift.pid=%s and shift.start_time>="%s" and shift.start_time<="%s" group by nozzle_number,start_time,mode';
       query=util.format(queryString,pid,from,to);
        }
        else{
            queryString='select u1.fname as supervisor,u2.fname as dsm,type,nozzle_number,shift.start_time as start_time,shift_nozzle_dsm.user_id as user_id,(dor-dcr-pump_test-self) as sale,shift_collection.id as id,mode,amount from user u1,shift,shift_nozzle_dsm,user u2,nozzle,shift_collection,collection_mode where shift.id=shift_nozzle_dsm.shift_id and u1.id=shift.supervisor_id and shift_nozzle_dsm.nozzle_id=nozzle.id and shift_nozzle_dsm.id=shift_collection.shift_nozzle_dsm_id and collection_mode.id=shift_collection.collection_mode_id and shift_nozzle_dsm.user_id=u2.id and shift.pid=%s group by nozzle_number,start_time,mode';
       query=util.format(queryString,pid);
        }
       
       var SalesDSMResult1=yield databaseUtils.executeQuery(query);
        var dsmDetails1=SalesDSMResult1;
        

        var queryString='select id,nozzle_number from nozzle where pid=%s';
        var query=util.format(queryString,pid);
        var nozzleCountResult=yield databaseUtils.executeQuery(query);

        queryString='select u.id,fname,lname,r.type as type from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.pid=%s and (r.type=\'supervisor\' or r.type=\'dsm\') and u.active=1';
        query=util.format(queryString,pid);
        var availablePersonResult=yield databaseUtils.executeQuery(query);

        queryString='select id,mode from collection_mode where pid=%s order by mode';
        query=util.format(queryString,pid);
        var paymentModes=yield databaseUtils.executeQuery(query);

        var queryString='select distinct(type) from tank_metadata where pid=%s';
        var query=util.format(queryString,pid);
        var tankType=yield databaseUtils.executeQuery(query);

        yield this.render('ifinance',{
    //     shiftDetails:shiftDetails,
    //     saleDetails:saleDetails,
    //    saleDetails1:saleDetails1,
    //     supervisorDetails:supervisorDetails,
    //   supervisorDetails1:supervisorDetails1,
    //    dsmDetails:dsmDetails,
        dsmDetails1:dsmDetails1,
       nozzleCountResult:nozzleCountResult,
       availablePersonResult:availablePersonResult,
       paymentModes:paymentModes,
       tankType:tankType,
       from:from,
       to:to,
       f1:f1,
       f2:f2,
       f3:f3,
       f4:f4,
       f5:f5,
       nozzle:nozzle,
       supervisor:supervisor,
       dsm:dsm,
       mode:mode,
       type:type,
    //   supdsmDetails:supdsmDetails 

        });
    },
    logout: function* (next) {
        var sessionid = this.cookies.get("SESSION_id");
        if(sessionid) {
            sessionUtils.deleteSession(sessionid);
        }
        this.cookies.set("SESSION_id", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    }
}


