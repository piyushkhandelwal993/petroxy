
var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');
module.exports={
login:function* (next){

    var user=this.request.body.email;
    var pwd=this.request.body.psw;

    var queryString='select u.id,u.pid,fname,lname,address,phone,email,password,pic,type from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and email="%s" and password="%s"';
    var query=util.format(queryString,user,pwd);
    var user=yield databaseUtils.executeQuery(query);
    var msg;
    console.log(user);
    if(user.length!=0 && (user[0].type=='admin' || user[0].type=='supervisor')){
        sessionUtils.saveUserInSession(user[0],this.cookies);
        this.redirect('/app/page2');
    }
    else{
        this.redirect('/app/login');
    }
},

signup: function* (next){
    var name=this.request.body.name;
    var email=this.request.body.email;
    var pwd=this.request.body.password;

    var queryString='insert into user (fname,email,password) values("%s","%s","%s")';
    var query=util.format(queryString,name,email,pwd);

    var errormsg;

    try{
        var result=yield databaseUtils.executeQuery(query);

        queryString='select * from user where id = %s';
        query = util.format(queryString,result.id);
        result=yield databaseUtils.executeQuery(query);

        var insertedUser=result[0];
        sessionUtils.saveUserInSession(insertedUser,this.cookies);
    }
    catch(e){
        if(e.code === 'ER_DUP_ENTRY'){
            errormsg='user already exists';
        } else {
            errormsg='error';
        }
    }
    if(errormsg){
        yield this.render('msg',{
            msg:errormsg,
        });
    }
    else{
        redirectUrl="/app/msg";
        this.redirect(redirectUrl);
    }
},
logout: function* (next) {
    var sessionid = this.cookies.get("SESSION_id");
    if(sessionid) {
        sessionUtils.deleteSession(sessionid);
    }
    this.cookies.set("SESSION_id", '', {expires: new Date(1), path: '/'});

    this.redirect('/app/login');
},
changepwd:function*(next){
    yield this.render('changepwd',{});
},
changepwd2:function*(next){
    var oldp=this.request.body.oldp;
    var newp1=this.request.body.newp1;
    var newp2=this.request.body.newp2;
    var re;
    var id=this.currentUser.id;
    console.log(oldp,newp1,newp2,id);
    if(newp1===newp2){
        console.log('yahaan aaya');
re=yield databaseUtils.executeQuery(util.format('update user set password="%s" where password="%s" and id=%s',newp1,oldp,id));
    }
    this.redirect('/app/login');
}
}

