var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
showicollectionPage: function* (next) {
    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}

    var queryString='select id,mode from collection_mode where pid=%s and del=0';
   var query=util.format(queryString,pid);

   var collectiontype=yield databaseUtils.executeQuery(query);

    yield this.render('icollectionmode',{
        
        collectiontype:collectiontype
});
},
showicollection2Page: function* (next) {

    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}
    var act=this.request.body.act.split(' ');
    if(parseInt(act[0])==1){
        var mode=this.request.body.mode;
        var queryString='insert into collection_mode(pid,mode) values(%s,"%s")';
        var query=util.format(queryString,pid,mode);
        var collectionresult=yield databaseUtils.executeQuery(query);
    }
    else if(parseInt(act[0])==3){
        var re=yield databaseUtils.executeQuery(util.format('update collection_mode set del=1 where id=%s',parseInt(act[2])));
    }

    
    this.redirect('icollectionmode');

},
}

