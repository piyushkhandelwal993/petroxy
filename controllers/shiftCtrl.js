var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {

    showShiftpage: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var queryString;
        var query;
        

        //Show all available shifts
        queryString='select shift_type,time(starttime) as start,time(endtime) as end from shift_metadata where pid=%s';
        query=util.format(queryString,pid);
        var availableShiftResult=yield databaseUtils.executeQuery(query);

        //Show all Available Supervisors
        queryString='select fname,lname from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.pid=%s and r.type=\'supervisor\' and u.active=1';
        query=util.format(queryString,pid);
        var availableSupervisorResult=yield databaseUtils.executeQuery(query);

        
        //Show all available nozzles
        queryString='select nozzle_number from nozzle where pid=1 and active=1';
        query=util.format(queryString,pid);
        var availableNozzleResult=yield databaseUtils.executeQuery(query);

        //Show all available DSM(s)
        queryString='select fname,lname from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.pid=%s and r.type=\'dsm\' and u.active=1';
        query=util.format(queryString,pid);
        var availableDSMResult=yield databaseUtils.executeQuery(query);

        //To end shift
        queryString='select nozzle_number,n.type,u1.fname as supervisor,u2.fname as dsm, start_time,end_time,dor  from shift_nozzle_dsm snd,user u1,user u2,shift s,nozzle n where n.id=snd.nozzle_id and snd.shift_id=s.id and u1.id=s.supervisor_id and u2.id=snd.user_id and dcr=0 and s.pid=%s';
        query=util.format(queryString,pid);
        var endShiftResult=yield databaseUtils.executeQuery(query);

        //To View Shift
        queryString='select s.id as id,sm.shift_type, u1.fname as supervisor, s.start_time,s.end_time from shift_metadata sm,shift s,user u1,shift_nozzle_dsm snd where sm.id=s.shift_meta_data_id and s.supervisor_id=u1.id and s.id=snd.shift_id and dcr!=0 and s.pid=%s group by s.id';
        query=util.format(queryString,pid);
        var viewShiftResult=yield databaseUtils.executeQuery(query);

        yield this.render('shift',{
            availableShiftResult:availableShiftResult,
            availableSupervisorResult:availableSupervisorResult,
            availableNozzleResult:availableNozzleResult,
            availableDSMResult:availableDSMResult,
            endShiftResult:endShiftResult,
            viewShiftResult:viewShiftResult
        });
    },
    
    logout: function* (next) {
        var sessionid = this.cookies.get("SESSION_id");
        if(sessionid) {
            sessionUtils.deleteSession(sessionid);
        }
        this.cookies.set("SESSION_id", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    },
    showiShiftPage: function* (next){

        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        queryString='select * from shift_metadata sm where not exists(select id from shift s where s.shift_meta_data_id=sm.id and date(start_time)=curdate()) and pid=%s';
        query=util.format(queryString,pid);
        var availableShiftResult=yield databaseUtils.executeQuery(query);

        //Show all Available Supervisors/DSM
        queryString='select u.id,fname,lname,r.type as type from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.pid=%s and (r.type=\'supervisor\' or r.type=\'dsm\') and u.active=1';
        query=util.format(queryString,pid);
        var availableSupervisorResult=yield databaseUtils.executeQuery(query);

        //Show all available nozzles
        queryString='select n.id as id,nozzle_number,dcr as dor,acr as aor from nozzle n,shift_nozzle_dsm snd where n.id=snd.nozzle_id and pid=%s and active=1 and snd.shift_id=(select max(id) from shift where pid=%s)';
        query=util.format(queryString,pid,pid);
        var availableNozzleResult=yield databaseUtils.executeQuery(query);

        if(availableNozzleResult.length==0){
            queryString='select id,nozzle_number,0 as dor,0 as aor from nozzle where pid=%s';
            query=util.format(queryString,pid);
            availableNozzleResult=yield databaseUtils.executeQuery(query);
        }

        //Query to show all available tanks(for opening reading)
        queryString='select type,close_reading from tank,tank_metadata where tank.tank_id=tank_metadata.id and shift_id=(select max(shift_id) from tank t,shift s where t.shift_id=s.id and s.pid=%s) and  tank_metadata.active=1 order by type';
        query=util.format(queryString,pid);
        var tanksResult=yield databaseUtils.executeQuery(query);

        if(tanksResult.length==0){
            tanksResult=yield databaseUtils.executeQuery(util.format('select name as type,qty as close_reading from product p,tank_metadata tm where p.name=tm.type and p.pid=%s',pid));
        }

        //Query to get the first shift that should be ended
        queryString='select fname,lname,shift_id,nozzle_id,user_id,dor,dcr,aor,acr,pump_test,self from shift_nozzle_dsm snd,user u where snd.shift_id=(select shift_id from shift_nozzle_dsm where dcr=0 and acr=0 and pid=%s order by id limit 1) and u.id=snd.user_id';
        query=util.format(queryString,pid);
        var endShiftResult=yield databaseUtils.executeQuery(query);
        
        //Query to get all payment modes
        queryString='select id,mode from collection_mode where pid=%s order by mode';
        query=util.format(queryString,pid);
        var paymentModes=yield databaseUtils.executeQuery(query);

        //var tanksResult1=yield databaseUtils.executeQuery(util.format('SELECT NAME AS TYPE,QTY AS OPEN_READING FROM PRODUCT P,TANK_METADATA TM WHERE P.NAME=TM.TYPE AND P.pid=%s',pid));
        var tanksResult1=yield databaseUtils.executeQuery(util.format('select type,open_reading from tank,tank_metadata where tank.tank_id=tank_metadata.id and shift_id=(select max(shift_id) from tank t,shift s where t.shift_id=s.id and s.pid=%s) order by type',pid));

        //For FIlter Purpose
        var dateactive=this.request.query.dateactive;
        var countactive;//=this.request.query.countactive;
        var from;
        var to;
        var count;
        var viewShiftResult;
        /*
        if(dateactive && countactive){
            from=this.request.query.from;
            to=this.request.query.to;
            count=this.request.query.count;
            queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s and s.start_time>="%s" and s.start_time<="%s" limit %s';
            query=util.format(queryString,pid,from,to,count);
            viewShiftResult=yield databaseUtils.executeQuery(query);
        }
        else */
            if(dateactive){
            from=this.request.query.from;
            to=this.request.query.to;
            queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s and s.start_time>="%s" and s.start_time<="%s"';
            query=util.format(queryString,pid,from,to);
            viewShiftResult=yield databaseUtils.executeQuery(query);
        }
        /*
        else if(countactive){
            count=this.request.query.count;
            queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s limit %s';
            query=util.format(queryString,pid,count);
            viewShiftResult=yield databaseUtils.executeQuery(query);
        }
        */
        else{
            queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s';
            query=util.format(queryString,pid);
            viewShiftResult=yield databaseUtils.executeQuery(query);
        }
        
        var dsmUsers;
        try{
        dsmUsers= yield databaseUtils.executeQuery(util.format('select distinct(fname),u.id as id from shift_nozzle_dsm snd,user u where u.id=snd.user_id and pid=%s and snd.shift_id=%s',pid,viewShiftResult[0].id));
        }catch(e){}
        var transports=yield databaseUtils.executeQuery(util.format('select * from transport where pid=%s',pid));

        var fuels=yield databaseUtils.executeQuery(util.format('select * from tank_metadata where pid=%s',pid));

        var modes=yield databaseUtils.executeQuery(util.format('select * from collection_mode where pid=%s order by mode',pid));
        console.log(availableNozzleResult);
        yield this.render('ishift',{
            availableShiftResult:availableShiftResult,
            availableSupervisorResult:availableSupervisorResult,
            availableNozzleResult:availableNozzleResult,
            endShiftResult:endShiftResult,
            paymentModes:paymentModes,
            viewShiftResult:viewShiftResult,
            tanksResult:tanksResult,
            countactive:countactive,
            dateactive:dateactive,
            from:from,
            to:to,
            count:count,
            transports:transports,
            fuels:fuels,
            tanksResult1:tanksResult1,
            dsmUsers:dsmUsers,
            modes:modes,
        });
    },
    showiShift2Page: function* (next){
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var stype=this.request.body.stype;
        var supervisor_id=this.request.body.supervisor;
        var act=this.request.body.act.split(" ");
        console.log('act length',act.length);

        var queryString;
        var query;
        if(act[0]=="1"){
        var uid;
        var aor;
        var dor;
        queryString='insert into shift (supervisor_id,start_time,end_time,shift_meta_data_id,pid)\
        select %s,concat(curdate()," ",time(starttime)),concat(curdate()," ",time(endtime)),id,%s\
        from shift_metadata where id=%s';
        console.log(pid,stype);
        query=util.format(queryString,supervisor_id,pid,stype);
            console.log(supervisor_id,pid,stype);
        var res=yield databaseUtils.executeQuery(query);
        var sid=res.insertId;
        console.log(res);
        
        queryString='select id from nozzle where pid=%s';
        query=util.format(queryString,pid);
        var nid=yield databaseUtils.executeQuery(query);
        for(var i=0;i<parseInt(act[1]);++i){
            uid=this.request.body['user'+i];
            aor=this.request.body['aor'+i];
            dor=this.request.body['dor'+i];
            console.log(uid);
            console.log(aor);
            console.log(dor);
            queryString='insert into shift_nozzle_dsm (shift_id,nozzle_id,user_id,aor,dor,acr,dcr,pump_test,self)\
            values(%s,%s,%s,%s,%s,%s,%s,%s,%s)';
            query=util.format(queryString,sid,nid[i].id,uid,aor,dor,0,0,0,0);
            console.log(sid,nid[i].id,uid,aor,dor,0,0,0,0);
            var r=yield databaseUtils.executeQuery(query);
        }

        var or;

        // tankid=yield databaseUtils.executeQuery(util.format('SELECT id FROM TANK_METADATA WHERE pid=%s ORDER BY TYPE',pid));
        // for(var i=0;i<parseInt(act[2]);++i){
        //     or=this.request.body['cr'+i];
        //     queryString='INSERT INTO TANK (TANK_id,SHIFT_id,OPEN_READING,CLOSE_READING,FUEL_RECEIVED)\
        //     VALUES(%s,%s,%s,%s,%s)';
        //     console.log(tankid[i].id,sid,or,0,0);
        //     query=util.format(queryString,tankid[i].id,sid,or,0,0);
        //     var re=yield databaseUtils.executeQuery(query);
        // }
        var r=yield databaseUtils.executeQuery(util.format('insert into tank (tank_id,shift_id,open_reading,close_reading,fuel_received) select id,%s,qty,%s,%s from tank_metadata where pid=%s',sid,0,0,pid));
       // var res=yield databaseUtils.executeQuery(util.format('INSERT INTO SHIFT_PRICE (pid,PRO_id,PRICE,OPEN_QTY,CLOSE_QTY,Sid) SELECT %s,id,PRICE,QTY,%s,%s FROM PRODUCT WHERE pid=%s',pid,0,sid,pid));
        }
        else if(act[0]=="2"){
            console.log(act[0],act[1],act[2],act[3],act[4],act[5],act[6]);
        var sid=parseInt(act[2]);
        var acr;
        var dcr;
        var pt;
        var self;
        queryString='select id from nozzle where pid=%s and active=1 order by nozzle_number';
        query=util.format(queryString,pid);
        var nid=yield databaseUtils.executeQuery(query);

        queryString='select id from collection_mode where pid=%s order by mode';
        query=util.format(queryString,pid);
        var cmid=yield databaseUtils.executeQuery(query);
        
        queryString='select id from shift_nozzle_dsm where shift_id=%s';
        query=util.format(queryString,sid);
        var sndid=yield databaseUtils.executeQuery(query);
        
        var tankid=yield databaseUtils.executeQuery(util.format('select id from tank_metadata where pid=%s order by type',pid));
        
        queryString='update shift_nozzle_dsm set acr=%s,dcr=%s,pump_test=%s,self=%s where shift_id=%s and nozzle_id=%s';

        for(var i=0;i<nid.length;++i){
            acr=this.request.body['acr'+i];
            dcr=this.request.body['dcr'+i];
            pt=this.request.body['pt'+i];
            self=this.request.body['self'+i];
            query=util.format(queryString,acr,dcr,pt,self,sid,nid[i].id);
            var r=yield databaseUtils.executeQuery(query);
        }

        // queryString='INSERT INTO SHIFT_COLLECTION (SHIFT_NOZZLE_DSM_id,COLLECTION_MODE_id,AMOUNT,pid) VALUES(%d,%d,%d,%d)';
        // for(var i=0;i<nid.length;++i){
        //     for(var j=0;j<cmid.length;++j){
        //         var amt=this.request.body['amt'+i+' '+j];
        //         query=util.format(queryString,sndid[i].id,cmid[j].id,amt,pid);
        //         var r=yield databaseUtils.executeQuery(query);
        //     }
        // }

        var cr;
        for(var i=0;i<tankid.length;++i){
            cr=this.request.body['cr'+i];
            queryString='update tank set close_reading=%s where tank_id=%s and shift_id=%s';
            console.log(cr,tankid[i].id,sid);
            query=util.format(queryString,cr,tankid[i].id,sid);
            var re=yield databaseUtils.executeQuery(query);
        }
        var rs=yield databaseUtils.executeQuery(util.format('select id from collection_mode where mode="bank" and pid=%s',pid));
            var bankid=rs[0].id;
            rs=yield databaseUtils.executeQuery(util.format('select id from collection_mode where mode="credit" and pid=%s',pid));
            var creditid=rs[0].id;
            console.log('bank',bankid,'credit',creditid);
        var leng=parseInt(act[4]);
        for(var i=0;i<=leng;++i){
           // var btnval=this.request.body['subname0'];
            //var btnval=this.request.body['subname'+i];
            var user;
            var mode;
            var modeid;
            var amount=this.request.body['amount'+i];
            if(this.request.body['user'+i]){
                user=this.request.body['user'+i];
                mode=this.request.body['mode'+i];
            }
            console.log('user',user,'mode',mode,'amount',amount);
            var arr;
            console.log('loop firse ',i);
            if(amount){
                rs=yield databaseUtils.executeQuery(util.format('select id from collection_mode where pid=%s and mode="%s"',pid,mode));
                if(rs.length>0) modeid=rs[0].id; 
                console.log('user ki id',modeid);   
             if(modeid==bankid){
                 console.log('bank mein aaya');
                    var acc=this.request.body['account'+i];
                    var s=yield databaseUtils.executeQuery(util.format('insert into shift_collection2 (user_id,collection_mode_id,amount,cbi,sid) values((select id from user where fname="%s" and pid=%s limit 1),(select id from collection_mode where mode="%s" and pid=%s limit 1),%s,%s,%s)',user,pid,mode,pid,amount,parseInt(acc),sid));
                    var s=yield databaseUtils.executeQuery(util.format('update bank_account set balance=balance+%s where id=%s',amount,parseInt(acc)));
                }
                else if(modeid===creditid) {
                    console.log('credit mein aaya');
                    while(this.request.body['party'+i]){
                        var partyid=this.request.body['party'+i];
                        var vehicle_no=this.request.body['vehicle'+i];
                        var invoice_no=this.request.body['invoice'+i];
                        amount=this.request.body['amount'+i];
                        //user=this.request.body['user'+i];
                        console.log(partyid);
                        ++i;//--leng;
                        var s=yield databaseUtils.executeQuery(util.format('insert into shift_collection2 (user_id,collection_mode_id,amount,cbi,sid) values((select id from user where fname="%s" and pid=%s limit 1),(select id from collection_mode where mode="%s" and pid=%s limit 1),%s,%s,%s)',user,pid,mode,pid,amount,parseInt(partyid),sid));
                        var s=yield databaseUtils.executeQuery(util.format('update credit set balance=balance+%s where id=%s',amount,parseInt(partyid)));
                        var s=yield databaseUtils.executeQuery(util.format('insert into credit_balance (vehicle_no,credit_id,pid,invoice_no,shift_id) values("%s",%s,%s,%s,%s)',vehicle_no,partyid,pid,invoice_no,sid));
                    }
                    --i;
                }
                else{
                    console.log('else mein gaya');
                    var s=yield databaseUtils.executeQuery(util.format('insert into shift_collection2 (user_id,collection_mode_id,amount,sid) values((select id from user where fname="%s" and pid=%s limit 1),(select id from collection_mode where mode="%s" and pid=%s limit 1),%s,%s)',user,pid,mode,pid,amount,sid));
                }
            }
            else{
            //  if(modeid!=creditid)
                leng++;
            }
            console.log('loop khatam');

        }

        
        //var fr=this.request.body.fuelreceived;
        leng=parseInt(act[5]);
        for(var i=0;i<=leng;++i){
            var ttno=this.request.body['ttno'+i];
            if(ttno){

            var transport=this.request.body['transport'+i];
            var product=this.request.body['product'+i];
            var invoiceno=this.request.body['invoiceno'+i];
            var invoicedate=this.request.body['invoicedate'+i];
            var invoiceamount=this.request.body['invoiceamount'+i];
            var invoiceqty=this.request.body['invoiceqty'+i];
            var invoicetemp=this.request.body['invoicetemp'+i];
            var rotemp=this.request.body['rotemp'+i];
            var invoicedensity=this.request.body['invoicedensity'+i];
            var rodensity=this.request.body['rodensity'+i];
            var tva=this.request.body['tva'+i];
            var shorttransport=this.request.body['short'+i];
            var fino=this.request.body['finvoiceno'+i];
            var fia=this.request.body['finvoiceamount'+i];
            queryString='insert into fuel_receipt (pid,tt,tid,pro_id,invoice_no,invoice_date,invoice_amount,invoice_qty,invoice_temp,ro_temp,invoice_density,ro_composite_density,tva,short_report,freight_invoice_no,freight_invoice_amount) \
            values(%s,%s,%s,%s,%s,"%s",%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)';
            query=util.format(queryString,pid,ttno,transport,product,invoiceno,invoicedate,invoiceamount,invoiceqty,invoicetemp,rotemp,invoicedensity,rodensity,tva,shorttransport,fino,fia);
            console.log(pid,ttno,transport,product,invoiceno,invoicedate,invoiceamount,invoiceqty,invoicetemp,rotemp,invoicedensity,rodensity,tva,shorttransport,fino,fia);
            res=yield databaseUtils.executeQuery(query);
            yield databaseUtils.executeQuery(util.format('update tank set fuel_received=fuel_received+%s where tank_id=%s and shift_id=%s',invoiceqty,product,sid));

            }
            else{
                leng++;
            }
        }
        leng=parseInt(act[6]);
        console.log('kirit ka dsm'+this.request.body['meradsm0']);
        var spquery=yield databaseUtils.executeQuery(util.format('insert into shift_price(pid,pro_id,price,open_qty,close_qty,sid)  select pid,id,price,qty,0,%s from product where pid=%s',sid,pid));
        for(var i=0;i<=leng;++i){
            console.log('loop no. ',i,' in non fuels');
            var nfdsm=this.request.body['meradsm'+i];
            console.log('nfdsm',nfdsm);
            if(nfdsm){
                var nfitem=this.request.body['nonfuelitem'+i];
                var nfqty=this.request.body['nonfuelqty'+i];
                var dsmid; var proid;
                console.log('ye waali line ',nfqty,nfitem);
                console.log('mere chaar item',nfitem,nfdsm,nfqty,sid);
                yield databaseUtils.executeQuery(util.format('insert into shift_product (pro_id,user_id,qty,sid) values(%s,%s,%s,%s)',nfitem,nfdsm,nfqty,sid));
                console.log('ek line khatam hui');
                yield databaseUtils.executeQuery(util.format('update product set qty=qty-%s where id=%s',nfqty,nfitem));
            }
            else{
                leng++;
            }
        }
        spquery=yield databaseUtils.executeQuery(util.format('update shift_price sp,product p set sp.close_qty=p.qty where sp.sid=%s and p.pid=%s',sid,pid));


        // if(fr){
            
        //     // queryString='INSERT INTO FUEL_RECEIPT (pid,TT,Tid,PRO_id,INVOICE_NO,INVOICE_DATE,INVOICE_AMOUNT,INVOICE_QTY,INVOICE_TEMP,RO_TEMP,INVOICE_DENSITY,RO_COMPOSITE_DENSITY,TVA,SHORT_REPORT,FREIGHT_INVOICE_NO,FREIGHT_INVOICE_AMOUNT) \
        //     // VALUES(%s,%s,%s,%s,%s,"%s",%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)';
        //     // query=util.format(queryString,pid,ttno,transport,product,invoiceno,invoicedate,invoiceamount,invoiceqty,invoicetemp,rotemp,invoicedensity,rodensity,tva,shorttransport,fino,fia);
        //     // res=yield databaseUtils.executeQuery(query);
            
        // }

        // var toupdate=yield databaseUtils.executeQuery(util.format('SELECT CM.id,SUM(AMOUNT) as CRDB FROM SHIFT S,SHIFT_NOZZLE_DSM SND,COLLECTION_MODE CM,SHIFT_COLLECTION SC WHERE S.id=SND.SHIFT_id AND SND.id=SC.SHIFT_NOZZLE_DSM_id AND SC.COLLECTION_MODE_id=CM.id AND S.pid=%s AND S.id=%s GROUP BY CM.MODE',pid,sid));
        // var oldbal=yield databaseUtils.executeQuery(util.format('SELECT B.MODE,BAL FROM COLLECTION_MODE CM,BALANCE B WHERE CM.pid=%s AND B.Sid=(SELECT MAX(Sid) FROM BALANCE WHERE pid=%s) AND CM.id=B.MODE ORDER BY B.MODE ',pid,pid));
        // var len;
        // //var res=yield databaseUtils.executeQuery(util.format('UPDATE PRODUCT P,( SELECT TYPE,CLOSE_READING FROM TANK T LEFT JOIN TANK_METADATA TM ON T.TANK_id=TM.id WHERE TM.pid=%s AND T.SHIFT_id=(SELECT MAX(id) FROM SHIFT WHERE pid=%s) ORDER BY TYPE) Q SET P.QTY= Q.CLOSE_READING WHERE P.NAME=Q.TYPE',pid,pid));

        // if(toupdate.length>oldbal.length) len=toupdate.length;
        // else len=oldbal.length;

        // for(var i=0;i<len;++i){
        //     if(toupdate.id==oldbal.MODE){
        //         console.log('yahaan error hai',toupdate[i].CRDB,toupdate[i].CRDB+oldbal[i].BAL,pid,oldbal[i].MODE,sid);
        //         res=yield databaseUtils.executeQuery(util.format('INSERT INTO BALANCE (CRDB,BAL,pid,MODE,Sid)\
        //         VALUES(%s,%s,%s,%s,%s)',toupdate[i].CRDB,toupdate[i].CRDB+oldbal[i].BAL,pid,oldbal[i].MODE,sid));
        //     }
        // }
        //Update Tank_metadata
        var re=yield databaseUtils.executeQuery(util.format('update tank t,tank_metadata tm set tm.qty=t.close_reading,t.price=tm.price where t.tank_id=tm.id and t.shift_id=%s',sid));

    }
        this.redirect('/app/ishift');

    },
    showishiftpage:function*(next){

        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}
        var openreadnotavail;
        //Query To get all available next shifts of current day
        queryString='select id,shift_type,concat(curdate(),\' \',time(starttime)) as starttime from shift_metadata sm where not exists(select id from shift s where s.shift_meta_data_id=sm.id and date(start_time)=curdate()) and pid=%s';
        query=util.format(queryString,pid);
        var availableShiftResult=yield databaseUtils.executeQuery(query);

        //Show all Available Supervisors/DSM
        queryString='select u.id,fname,lname,r.type as type from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.pid=%s and (r.type=\'supervisor\' or r.type=\'dsm\') and u.active=1';
        query=util.format(queryString,pid);
        var availableSupervisorResult=yield databaseUtils.executeQuery(query);

        //Show all available nozzles
        queryString='select n.id as id,nozzle_number,dcr as dor,acr as aor from nozzle n,shift_nozzle_dsm snd where n.id=snd.nozzle_id and pid=%s and active=1 and snd.shift_id=(select max(id) from shift where pid=%s)';
        
        query=util.format(queryString,pid,pid);
        var availableNozzleResult=yield databaseUtils.executeQuery(query);

        if(availableNozzleResult.length==0){
            var temp=yield databaseUtils.executeQuery(util.format('select * from shift s left join shift_nozzle_dsm snd on s.id=snd.shift_id where pid=%s',pid));
            if(temp.length==0){
            queryString='select id,nozzle_number,0 as dor,0 as aor from nozzle where pid=%s';
            query=util.format(queryString,pid);
            availableNozzleResult=yield databaseUtils.executeQuery(query);
            }
            else{
                openreadnotavail='Not Available';
            }
        }

        //Query to show all available tanks(for opening reading)
        // queryString='SELECT TYPE,CLOSE_READING FROM TANK,TANK_METADATA WHERE TANK.TANK_id=TANK_METADATA.id AND SHIFT_id=(SELECT MAX(SHIFT_id) FROM TANK T,SHIFT S WHERE T.SHIFT_id=S.id AND S.pid=%s) AND  TANK_METADATA.ACTIVE=1 ORDER BY TYPE';
        // query=util.format(queryString,pid);
        // var tanksResult=yield databaseUtils.executeQuery(query);

        // if(tanksResult.length==0){
        //     tanksResult=yield databaseUtils.executeQuery(util.format('SELECT NAME AS TYPE,QTY AS CLOSE_READING FROM PRODUCT P,TANK_METADATA TM WHERE P.NAME=TM.TYPE AND P.pid=%s',pid));
        // }

        //FOr getting Openinjg Reading
        tanksResult=yield databaseUtils.executeQuery(util.format('select * from tank_metadata where pid=%s order by type',pid));

        //Query to get the first shift that should be ended
        queryString='select fname,lname,shift_id,nozzle_id,user_id,dor,dcr,aor,acr,pump_test,self from shift_nozzle_dsm snd,user u where snd.shift_id=(select shift_id from shift_nozzle_dsm where dcr=0 and acr=0 and pid=%s order by id limit 1) and u.id=snd.user_id order by nozzle_id';
        query=util.format(queryString,pid);
        var endShiftResult=yield databaseUtils.executeQuery(query);
        
        //Query to get all payment modes
        queryString='select id,mode from collection_mode where pid=%s order by mode';
        query=util.format(queryString,pid);
        var paymentModes=yield databaseUtils.executeQuery(query);

        //var tanksResult1=yield databaseUtils.executeQuery(util.format('SELECT NAME AS TYPE,QTY AS OPEN_READING FROM PRODUCT P,TANK_METADATA TM WHERE P.NAME=TM.TYPE AND P.pid=%s',pid));
        var tanksResult1=yield databaseUtils.executeQuery(util.format('select type,open_reading from tank,tank_metadata where tank.tank_id=tank_metadata.id and shift_id=(select max(shift_id) from tank t,shift s where t.shift_id=s.id and s.pid=%s) order by type',pid));

        //For FIlter Purpose
        // var dateactive=this.request.query.dateactive;
        // var countactive=this.request.query.countactive;
        var from;
        var to;
        var count;
        var viewShiftResult
        if(from && to){
            from=this.request.query.from;
            to=this.request.query.to;
            queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s and s.start_time>="%s" and s.start_time<="%s" limit %s';
            query=util.format(queryString,pid,from,to,count);
            viewShiftResult=yield databaseUtils.executeQuery(query);
        }
        else{
            queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s';
            query=util.format(queryString,pid);
            viewShiftResult=yield databaseUtils.executeQuery(query);
        }
        
        var dsmUsers;
        try{
        dsmUsers= yield databaseUtils.executeQuery(util.format('select distinct(fname) from user u left join shift_nozzle_dsm snd on u.id=snd.user_id where snd.shift_id=(select shift_id from shift_nozzle_dsm where pid=%s and dcr=0 limit 1)',pid));
        }catch(e){}
        var transports=yield databaseUtils.executeQuery(util.format('select * from transport where pid=%s',pid));

        var fuels=yield databaseUtils.executeQuery(util.format('select * from tank_metadata where pid=%s',pid));

        var modes=yield databaseUtils.executeQuery(util.format('select * from collection_mode where pid=%s order by mode',pid));

        yield this.render('ishift',{
            availableShiftResult:availableShiftResult,
            availableSupervisorResult:availableSupervisorResult,
            availableNozzleResult:availableNozzleResult,
            endShiftResult:endShiftResult,
            paymentModes:paymentModes,
            viewShiftResult:viewShiftResult,
            tanksResult:tanksResult,
         //   dateactive:dateactive,
            from:from,
            to:to,
            count:count,
            transports:transports,
            fuels:fuels,
            tanksResult1:tanksResult1,
            dsmUsers:dsmUsers,
            modes:modes,
        });
    },




    showiSchedulePage: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var QueryString;
        var query;
        QueryString='select shift_metadata.id,starttime,endtime,shift_type,sid,fname,lname,shift_metadata.active  from shift_metadata,user where user.id=shift_metadata.sid and shift_metadata.del=0 and user.pid=%s';
        query=util.format(QueryString,pid);
        var ScheduleResult=yield databaseUtils.executeQuery(query);
        //queryString='SELECT DISTINCT(FNAME),U1.id AS id FROM USER U1,SHIFT_METADATA SM WHERE U1.id=SM.Sid AND SM.pid=%s';
        queryString='select u.id as id,fname from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and r.type in (\'supervisor\',\'admin\') and u.del=0 and u.pid=%s';
        query=util.format(queryString,pid);
        var availableSupervisorResult=yield databaseUtils.executeQuery(query);
        //var QueryString='SELECT DISTINCT(SHIFT_TYPE) FROM SHIFT_METADATA WHERE pid=%s';
        //var query=util.format(QueryString,pid);
        //var ShiftType=yield databaseUtils.executeQuery(query);
        yield this.render('ischedule',{
            ScheduleResult:ScheduleResult,
            availableSupervisorResult:availableSupervisorResult,
          //  ShiftType:ShiftType,
    });
},
showiSchedule2Page: function* (next) {
    var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

    var scid=this.request.body.scid;
    var sid=this.request.body.sid;
    var starttime=this.request.body.starttime;
    var endtime=this.request.body.endtime;
    var act=this.request.body.act.split(' ');
    if(parseInt(act[0])==1){
        //try{
       // pid=this.request.body.pid;
       // var starttime=this.request.body.starttime;
       // var endtime=this.request.body.endtime;
        var shifttype =this.request.body.shifttype;
        //var sid =this.request.body.sid;
        var queryString='insert into shift_metadata (pid,starttime,endtime,shift_type,sid) \
        values(%s,concat(curdate(),"%s"),concat(curdate(),"%s"),%s,%s)';
        console.log(pid,starttime,endtime,shifttype,sid);
        var query=util.format(queryString,pid,' '+starttime,' '+endtime,shifttype,sid);
        var res=yield databaseUtils.executeQuery(query);
        //}
        // catch(e){
        //     pid=1;
        // }
    }
    else if(parseInt(act[0])==2){
        var active=this.request.body.active;
       scid=parseInt(act[1]);
       console.log(starttime,' '+endtime,sid,scid);
       if(starttime && endtime){
        var queryString='update shift_metadata set starttime=concat(curdate(),"%s"),endtime=concat(curdate(),"%s"), sid=%s where id=%s';
        var query=util.format(queryString,' '+starttime,' '+endtime,sid,scid);
        var res=yield databaseUtils.executeQuery(query);
       }
       else{
           var re=yield databaseUtils.executeQuery(util.format('update shift_metadata set active=%s where id=%s',active,scid));
       }

    }
    else{
        console.log('act print karwaya '+act);
            var res=yield databaseUtils.executeQuery(util.format('update shift_metadata set del=1 where id=%s',parseInt(act[2])));

    }
this.redirect('ischedule');
/*
    yield this.render('ischedule',{
        ScheduleResult:ScheduleResult,
        availableSupervisorResult:availableSupervisorResult,
});*/
},
showshift2page: function*(next){

    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}



    var openreadnotavail;
    //Query To get all available next shifts of current day
    queryString='select id,shift_type,concat(curdate(),\' \',time(starttime)) as starttime from shift_metadata sm where not exists(select id from shift s where s.shift_meta_data_id=sm.id and date(start_time)=curdate()) and pid=%s';
    query=util.format(queryString,pid);
    var availableShiftResult=yield databaseUtils.executeQuery(query);

    //Show all Available Supervisors/DSM
    queryString='select u.id,fname,lname,r.type as type from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.pid=%s and (r.type=\'supervisor\' or r.type=\'dsm\') and u.active=1';
    query=util.format(queryString,pid);
    var availableSupervisorResult=yield databaseUtils.executeQuery(query);

    //Show all available nozzles
    queryString='select n.id as id,nozzle_number,dcr as dor,acr as aor from nozzle n,shift_nozzle_dsm snd where n.id=snd.nozzle_id and pid=%s and active=1 and snd.shift_id=(select max(id) from shift where pid=%s)';
    
    query=util.format(queryString,pid,pid);
    var availableNozzleResult=yield databaseUtils.executeQuery(query);

    if(availableNozzleResult.length==0){
        console.log('available supervisor result 0');
        var temp=yield databaseUtils.executeQuery(util.format('select * from shift s left join shift_nozzle_dsm snd on s.id=snd.shift_id where pid=%s',pid));
        if(temp.length==0){
        queryString='select id,nozzle_number,0 as dor,0 as aor from nozzle where pid=%s';
        query=util.format(queryString,pid);
        availableNozzleResult=yield databaseUtils.executeQuery(query);
        }
        else{
            openreadnotavail='Not Available';
        }
    }
    tanksResult=yield databaseUtils.executeQuery(util.format('select * from tank_metadata where pid=%s order by type',pid));

    yield this.render('shift2',{
        availableShiftResult:availableShiftResult,
        availableSupervisorResult:availableSupervisorResult,
        availableNozzleResult:availableNozzleResult,
        tanksResult:tanksResult,
    });
},
showshift3page:function*(next){

    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}
    
    var from;
    var to;
    var viewShiftResult;
    from=this.request.query.from;
    to=this.request.query.to;
    if(from && to){
        console.log('from && to');

        count=this.request.query.count;
        queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s and s.start_time>="%s" and s.start_time<="%s"';
        query=util.format(queryString,pid,from,to);
        viewShiftResult=yield databaseUtils.executeQuery(query);
    }
    else{
        console.log('no from and to');
        queryString='select s.id as id,start_time,end_time,fname,lname from shift s,user u where u.id=s.supervisor_id and s.pid=%s';
        query=util.format(queryString,pid);
        viewShiftResult=yield databaseUtils.executeQuery(query);
    }
    yield this.render('shift3',{
        viewShiftResult:viewShiftResult,
        from:from,to:to,
    });
}
}


