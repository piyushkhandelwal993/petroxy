var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    
    showReportpage: function* (next) {
        var msg;
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;
    msg='Login FIrst';

}

        var queryString;
        var query;
        var msg;
        // QUERY FOR GETTING SHIFT DETAILS
        queryString='select shift_id as id,shift_type,date(starttime) as date,u1.fname as supervisor from shift_nozzle_dsm snd,shift s,user u1,shift_metadata sm where shift_meta_data_id=sm.id and supervisor_id=u1.id and snd.shift_id=s.id and dcr!=0 and s.pid=%s order by shift_id desc limit 1';
        query = util.format(queryString,pid);
        var shiftDetailsResult=yield databaseUtils.executeQuery(query);
        console.log(shiftDetailsResult);

        if(shiftDetailsResult.length==0){
            msg='No Reports Found...!!!';
            yield this.render('report',{
                msg:msg,
            });
        }
        else{

        var sid;
        //Fetching Shift id from Query Parameter for Particuler shift
        sid=this.request.query.sid;
        console.log('Query parameter se li sid hai...',sid);
        if(!sid){
        //Fetched Shift id
        var sid=shiftDetailsResult[0].id;
        }
        else{
            queryString='select shift_id as id,shift_type,date(starttime) as date,u1.fname as supervisor from shift_nozzle_dsm snd,shift s,user u1,shift_metadata sm where shift_meta_data_id=sm.id and supervisor_id=u1.id and snd.shift_id=s.id and s.pid=%s and s.id=%s';
            query = util.format(queryString,pid,sid);
            shiftDetailsResult=yield databaseUtils.executeQuery(query);
        }
        console.log(sid);

        // QUERY FOR ELECTRONIC TOTALIZER 
        queryString='select n.nozzle_number as nozzle, n.type as product,u1.fname as assignted_to,dor,dcr,pump_test,(dcr-dor-pump_test) as sale from shift_nozzle_dsm snd,nozzle n,user u1 where snd.nozzle_id=n.id and snd.user_id=u1.id and snd.shift_id=%s and n.pid=%s';
        query=util.format(queryString,sid,pid);
        var elecTotalizerResult=yield databaseUtils.executeQuery(query);

        queryString='select n.type as product, sum(dcr-dor) as disp,sum(pump_test) as pumptest,sum(dcr-dor-pump_test) as sale from shift_nozzle_dsm snd,nozzle n,user u1 where snd.nozzle_id=n.id and snd.user_id=u1.id and snd.shift_id=%s and n.pid=%s group by n.type order by n.type';
        query=util.format(queryString,sid,pid);
        var elecTotalizerResult1=yield databaseUtils.executeQuery(query);

        // QUERY FOR MANUAL TOTALIZER 
        queryString='select n.nozzle_number as nozzle, n.type as product,u1.fname as assignted_to,aor,acr,pump_test,(acr-aor-pump_test) as sale from shift_nozzle_dsm snd,nozzle n,user u1 where snd.nozzle_id=n.id and snd.user_id=u1.id and snd.shift_id=%s and n.pid=%s';
        query=util.format(queryString,sid,pid);
        var manTotalizerResult=yield databaseUtils.executeQuery(query);

        queryString='select n.type as product, sum(acr-aor) as disp,sum(pump_test) as pumptest,sum(acr-aor-pump_test) as sale from shift_nozzle_dsm snd,nozzle n,user u1 where snd.nozzle_id=n.id and snd.user_id=u1.id and snd.shift_id=%s and n.pid=%s group by n.type';
        query=util.format(queryString,sid,pid);
        var manTotalizerResult1=yield databaseUtils.executeQuery(query);

        // QUERY FOR DIFF  BETWEEN ELEC. AND MANUAL TOTALIZER 
        queryString='select n.type as product,sum(pump_test) as pumptest,(sum(aor-acr-pump_test)-sum(dor-dcr-pump_test)) as difference from shift_nozzle_dsm snd,nozzle n,user u1 where snd.nozzle_id=n.id and snd.user_id=u1.id and snd.shift_id=%s and n.pid=%s group by n.type order by n.type';
        query=util.format(queryString,sid,pid);
        var emdiffResult=yield databaseUtils.executeQuery(query);
        
        // // QUERY FOR GETTING VOLUME THROUGH HEIGHT
        // queryString='SELECT SP.Sid, P.NAME,SP.QTY AS QTY,CMV+MMV*(SP.QTY-CAST(SP.QTY AS SIGNED))*10 AS VOLUME FROM SHIFT_PRICE SP,PRODUCT P,DATASHEET D,TANK_METADATA TM WHERE P.id=SP.PRO_id AND D.pid=%s AND D.TANK_METADATA_id=TM.id AND TM.TYPE=P.NAME AND HEIGHT=CAST(SP.QTY AS UNSIGNED)\
        //  AND (SP.Sid=(SELECT SHIFT_id AS Sid FROM SHIFT_NOZZLE_DSM SND,SHIFT S,USER U1,SHIFT_METADATA SM WHERE SHIFT_META_DATA_id=SM.id AND SUPERVISOR_id=U1.id AND SND.SHIFT_id=S.id AND DCR!=0 AND ACR!=0 AND S.pid=%s GROUP BY SHIFT_id ORDER BY SHIFT_id DESC LIMIT 1 OFFSET 1) OR SP.Sid=%s) \
        //  ORDER BY P.NAME,SP.Sid';
        //  console.log(pid,sid);
        // query=util.format(queryString,pid,pid,sid);
        // //console.log(tankheightQuery);
        // var tankheightResult=yield databaseUtils.executeQuery(query);
        // queryString='SELECT SP.Sid,TM.TYPE, CLOSE_READING,(CMV+MMV*(CLOSE_READING-CAST(CLOSE_READING AS SIGNED))*10) AS CLOSE FROM TANK T LEFT JOIN TANK_METADATA TM ON T.TANK_id=TM.id LEFT JOIN PRODUCT P ON TM.TYPE=P.NAME LEFT JOIN SHIFT_PRICE SP ON P.id=SP.PRO_id LEFT JOIN DATASHEET D ON T.TANK_id=D.TANK_METADATA_id WHERE TM.pid=%s AND D.HEIGHT=CAST(CLOSE_READING AS SIGNED) AND SP.Sid=%s AND T.SHIFT_id=%s ORDER BY TM.TYPE';
        // query=util.format(queryString,pid,sid,sid);
        // var tankheightResult1=yield databaseUtils.executeQuery(query);

        // queryString='SELECT SP.Sid,TM.TYPE, OPEN_READING,(CMV+MMV*(OPEN_READING-CAST(OPEN_READING AS SIGNED))*10) AS OPEN FROM TANK T LEFT JOIN TANK_METADATA TM ON T.TANK_id=TM.id LEFT JOIN PRODUCT P ON TM.TYPE=P.NAME LEFT JOIN SHIFT_PRICE SP ON P.id=SP.PRO_id LEFT JOIN DATASHEET D ON T.TANK_id=D.TANK_METADATA_id WHERE TM.pid=%s AND D.HEIGHT=CAST(OPEN_READING AS SIGNED) AND SP.Sid=%s AND T.SHIFT_id=%s ORDER BY TM.TYPE';
        // query=util.format(queryString,pid,sid,sid);
        // var tankheightResult2=yield databaseUtils.executeQuery(query);

        var tankheightResult1=yield databaseUtils.executeQuery(util.format('select t.price,type,tank_id,open_reading,fuel_received,(cmv+mmv*(open_reading-cast(open_reading as signed))*10) as open from tank t left join tank_metadata tm on t.tank_id=tm.id left join datasheet d on tm.id=d.tank_metadata_id where cast(open_reading as signed)=height and  tm.pid=%s and t.shift_id=%s',pid,sid));
        var tankheightResult2=yield databaseUtils.executeQuery(util.format('select t.price,type,tank_id,close_reading,(cmv+mmv*(close_reading-cast(close_reading as signed))*10) as close from tank t left join tank_metadata tm on t.tank_id=tm.id left join datasheet d on tm.id=d.tank_metadata_id where cast(close_reading as signed)=height and  tm.pid=%s and t.shift_id=%s',pid,sid));

        console.log(tankheightResult1.length,tankheightResult2.length);

        // QUERY FOR PRODUCT PRICE 
        queryString='select type,price from tank_metadata where pid=%s order by type';
        query=util.format(queryString,pid);
        var priceResult=yield databaseUtils.executeQuery(query);

        // SS CHECKSHEET QUERY FOR ESTIMATING COLLECTION 
        queryString='select nozzle_number,u.fname as assigned_to,n.type, (dcr-dor-pump_test-self) as sale,(dcr-dor-pump_test-self)*price as collection from shift s, shift_nozzle_dsm snd,nozzle n,product p,user u where s.id=snd.shift_id and snd.user_id=u.id and n.id=snd.nozzle_id and n.type=p.name and s.id=%s and p.pid=%s order by nozzle_number';
        query=util.format(queryString,sid,pid);
        var estResult=yield databaseUtils.executeQuery(query);

        // CHECKSHEET QUERY FOR ACTUAL COLLECTION (TO SHOW) 
        queryString='select nozzle_number,u.fname,cm.mode,sc.amount from shift s,shift_nozzle_dsm snd,nozzle n,shift_collection sc,collection_mode cm,user u where s.id=snd.shift_id and snd.nozzle_id=n.id and snd.id=sc.shift_nozzle_dsm_id and sc.collection_mode_id=cm.id and u.id=snd.user_id and s.pid=%s and s.id=%s order by nozzle_number';
        query=util.format(queryString,pid,sid);
        var actResult=yield databaseUtils.executeQuery(query);
        
        var salemodewise=yield databaseUtils.executeQuery(util.format('select fname,mode,sum(amount) as sale from user u,collection_mode cm ,shift_collection2 sc where u.id=sc.user_id and cm.id=sc.collection_mode_id and sc.sid=%s and cm.pid=%s group by fname,mode',sid,pid));

        // CHECKSHEET QUERY FOR ACTUAL COLLECTION (TO CALCULATE) 
        queryString='select nozzle_number,u.fname as assigned_to ,sum(sc.amount) as collection from shift s,shift_nozzle_dsm snd,nozzle n,shift_collection sc,collection_mode cm,user u where s.id=snd.shift_id and snd.nozzle_id=n.id and snd.id=sc.shift_nozzle_dsm_id and sc.collection_mode_id=cm.id and u.id=snd.user_id and s.pid=%s and s.id=%s group by nozzle_number order by nozzle_number;';
        query=util.format(queryString,pid,sid);
        var actResult1=yield databaseUtils.executeQuery(query);

        var msg;

        var res1=yield databaseUtils.executeQuery(util.format('select u.fname,sum(dcr-dor-pump_test-self)*t.price as est from user u left join shift_nozzle_dsm snd on u.id=snd.user_id left join nozzle n on n.id=snd.nozzle_id left join tank_metadata tm on n.type=tm.type left join tank t on t.tank_id=tm.id where snd.shift_id=%s and tm.pid=%s group by u.id order by u.fname',sid,pid));
        var res2=yield databaseUtils.executeQuery(util.format('select fname,sum(amount) as act from user u left join shift_collection2 sc on u.id=sc.user_id left join collection_mode cm on cm.id=sc.collection_mode_id where u.pid=%s and sc.sid=%s group by u.id',pid,sid));

        var nonfuelsale=yield databaseUtils.executeQuery(util.format('select distinct(user_id),a.amt from shift_nozzle_dsm snd left join (select id,amt from user left join (select user_id,sum(qty*price) as amt from shift_product sp,shift_price spr where sp.pro_id=spr.pro_id and spr.pid=%s group by user_id) s on user.id=s.user_id) a on snd.user_id=a.id where  snd.shift_id=%s',pid,sid));

        yield this.render('report',{
            shiftDetailsResult:shiftDetailsResult[0],
            elecTotalizerResult:elecTotalizerResult,
            elecTotalizerResult1:elecTotalizerResult1,
            manTotalizerResult:manTotalizerResult,
            manTotalizerResult1:manTotalizerResult1,
            emdiffResult:emdiffResult,
            res1:res1,
            res2:res2,
            nonfuelsale:nonfuelsale,
        //    tankheightResult:tankheightResult,
            priceResult:priceResult,
            estResult:estResult,
            actResult:actResult,
            actResult1:actResult1,
            msg:msg,
            salemodewise:salemodewise,
            tankheightResult1:tankheightResult1,
            tankheightResult2:tankheightResult2,
        });
    }
    
    }

}


