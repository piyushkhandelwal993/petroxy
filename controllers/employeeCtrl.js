var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
    
    showEmployeePage: function* (next) {
        var sessionid = this.cookies.get("SESSION_id");
        var user=sessionUtils.getCurrentUser(sessionid,function(err,res){
            if(res) console.log(res);
            else console.log("no user");
        });

    },
    showEmployee1Page : function *(next){

        yield this.render('employee',{
        });

    },
    showiemployeePage: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}

        var QueryString;
        var query;
        var employeeResult;
        var tt='All';
            var etype=this.request.query.etype;
            if (etype=='All'){
                queryString=' select user.id as id,fname,lname,type,address,phone,email,adhaar,active,pic from user,role,user_role where user.id=user_role.user_id and role.id=user_role.role_id and user.pid=%s and del=0';
                query = util.format(queryString,pid);
                tt='All';
            }
            else{
                queryString='select user.id as id,fname,lname,type,address,phone,email,adhaar,active,pic from user,role,user_role where user.id=user_role.user_id and role.id=user_role.role_id and user.pid=%s and type="%s" and del=0';
                query=util.format(queryString,pid,etype);
                tt=etype;
            }
            employeeResult=yield databaseUtils.executeQuery(query);
            if(employeeResult.length==0){
            QueryString='select user.id as id,fname,lname,type,address,phone,email,adhaar,active,pic from user,role,user_role where user.id=user_role.user_id and role.id=user_role.role_id and user.pid=%s and del=0';
            query=util.format(QueryString,pid);
            employeeResult=yield databaseUtils.executeQuery(query);
            tt='All';
        }

    
        //employeeResult=yield databaseUtils.executeQuery(query);
        var QueryString='select distinct(type),id from role where pid=%s';
        var query=util.format(QueryString,pid);
        var employeerole=yield databaseUtils.executeQuery(query);
        yield this.render('iemployee',{
            employeeResult:employeeResult,
            employeeRole:employeerole,
            tt:tt,
    });
},
showiEmployee2Page: function* (next) {
    var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}



    var act;
    var tact=this.request.body.act;
    if(tact) act=tact.split(' ');
    else 
    act=this.request.body.fields.act.split(' ');

    console.log(act,"ye act hai");


    var etype;
    var employeeResult;
    var employeeRole;
    if(act[0]=="2"){
        var fname=this.request.body.fields.fname;
        var lname=this.request.body.fields.lname;
        var type=this.request.body.fields.type;
        var address=this.request.body.fields.address;
        var email=this.request.body.fields.email;
        var phone=this.request.body.fields.phone;
        var adhaar=this.request.body.fields.adhaar;
        var filee=this.request.body.files;
        var pp;
        try{
            var pic=filee.pic.path.split('/');
            pp=pic[pic.length-1];
        }catch(e){
            pp='abcd';
        }
        var pwd=this.request.body.fields.psw;
        var active=this.request.body.fields.active;
        

        console.log(fname,lname,address,email,phone,adhaar,pwd,pp,parseInt(act[1]));
        console.log(type);
        if(fname && lname &&type && address && email && phone && adhaar && pwd && pic)
        {
    queryString='update user set fname="%s",lname="%s",address="%s",email="%s",phone=%s,adhaar=%s,password="%s",active=%s,pic="%s" where id=%s';
    query=util.format(queryString,fname,lname,address,email,phone,adhaar,pwd,active,pic[pic.length-1],parseInt(act[1]));
    var res=yield databaseUtils.executeQuery(query);
        }

    else if (active)
    {
        var res=yield databaseUtils.executeQuery(util.format('update user set active=%s where id=%s',active,parseInt(act[1])));

    }
    
    var rid;
    res=yield databaseUtils.executeQuery(util.format('select id from role where pid=%s and type="%s"',pid,type));
        if(res.length==0){
            var res=yield databaseUtils.executeQuery(util.format('insert into role (type,pid) values("%s",%s)',type,pid));
            rid=res.insertid;
        }
        else{
            rid=res[0].id;
        }
        console.log(rid,parseInt(act[1]));
    queryString='update user_role set role_id=%s where user_id=%s';
    query=util.format(queryString,rid,parseInt(act[1]));
    res=yield databaseUtils.executeQuery(query);
    }
    else if(act[0]=="1"){

        var fname=this.request.body.fields.fname;
        var lname=this.request.body.fields.lname;
        var type=this.request.body.fields.type;
        var address=this.request.body.fields.address;
        var email=this.request.body.fields.email;
        var phone=this.request.body.fields.phone;
        var adhaar=this.request.body.fields.adhaar;
        var filee=this.request.body.files;
        var pp;
        try{
            var pic=filee.pic.path.split('/');
            pp=pic[pic.length-1];
        }catch(e){
            pp='abcd';
            
        }
        var pwd=this.request.body.fields.pwd;

        queryString='insert into user (pid,fname,lname,address,email,phone,adhaar,password,pic) \
        values(%s,"%s","%s","%s","%s",%s,%s,"%s","%s")';
        query=util.format(queryString,pid,fname,lname,address,email,phone,adhaar,pwd,pp);
        var res=yield databaseUtils.executeQuery(query);
        var uid=res.insertid;

        res=yield databaseUtils.executeQuery(util.format('select id from role where pid=%s and type="%s"',pid,type));
        if(res.length==0){
            var res=yield databaseUtils.executeQuery(util.format('insert into role (type,pid) values("%s",%s)',type,pid));
            rid=res.insertid;
        }
        else{
            rid=res[0].id;
        }
        console.log(pid,uid,rid);
        var r=yield databaseUtils.executeQuery(util.format('insert into user_role (pid,user_id,role_id)\
        values(%s,%s,%s)',pid,uid,rid));
    }
    

    else{
        console.log(act);
        
        if(parseInt(act[0])==3){
        var res=yield databaseUtils.executeQuery(util.format('update user set del=1 where id=%s',parseInt(act[1])));
        console.log('yaha aaya');
        }
    }


    this.redirect('iemployee');
    /*
    yield this.render('iemployee',{
        employeeResult:employeeResult,
        employeeRole:employeeRole,
        tt:tt,
});*/
}
    
    
}


