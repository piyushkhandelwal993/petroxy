var sessionUtils = require('../utils/sessionUtils');

var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');
var redisUtils = require('../utils/redisUtils');

module.exports = {
    showDashboardPage: function* (next) {
        var pid;
try{ pid=this.currentUser.pid;}
catch(e){pid=0;}
        //var pid=1;
        //Keeping this query just for sample query
        /*
        var queryString='select * from user where id = %s';
        var query=util.format(queryString,userid);
        var result=yield databaseUtils.executeQuery(query);
        var userDetails=result[0];
        */

        var petrolpumpQueryString='select * from petrolpump where id=%s';
        var petrolpumpQuery=util.format(petrolpumpQueryString,pid);
        var petrolpumpResult=yield databaseUtils.executeQuery(petrolpumpQuery);

        //For total sale of HSD and MS
        var saleQueryString='select n.type,sum(dor-dcr-pump_test-self) as sale from shift s,shift_nozzle_dsm snd,nozzle n where snd.nozzle_id=n.id and snd.shift_id=s.id and date(s.start_time)=curdate() and s.pid=%s group by n.type';
        var saleQuery=util.format(saleQueryString,pid);
        var saleResult=yield databaseUtils.executeQuery(saleQuery);

        //For toal quantity left of HSD and MS
        var quantityQueryString='select n.type,sum(dcr) as qty_left from shift s,shift_nozzle_dsm snd,nozzle n where snd.nozzle_id=n.id and snd.shift_id=s.id and date(s.start_time)=curdate() and s.pid=%s group by s.id,n.type limit 2';
        var quantityQuery=util.format(quantityQueryString,pid);
        var quantityResult=yield databaseUtils.executeQuery(quantityQuery);


        var shiftQueryString='select s.start_time,s.end_time,u1.fname as supervisor from shift s,user u1 where s.supervisor_id=u1.id and s.pid=%s order by s.id desc limit 1';
        var shiftQuery=util.format(shiftQueryString,pid);
        var shiftResult=yield databaseUtils.executeQuery(shiftQuery);

        var nozzleCountQueryString='select count(*) as c from nozzle where pid=%s';
        var nozzleCountQuery=util.format(nozzleCountQueryString,pid);
        var nozzleCountResult=yield databaseUtils.executeQuery(nozzleCountQuery);

        var shiftDetailsQueryString='select u2.fname as dsm,nozzle_number,dor from shift s,nozzle n,shift_nozzle_dsm snd, user u2 where snd.nozzle_id=n.id and u2.id=snd.user_id and s.id=snd.shift_id and s.pid=%s order by s.id desc limit %s';
        var shiftDetailsQuery=util.format(shiftDetailsQueryString,pid,nozzleCountResult[0].C);
        var shiftDetailsResult=yield databaseUtils.executeQuery(shiftDetailsQuery);

        var tankCountQueryString='select count(*) as C from tank_metadata where pid="%s"';
        var tankCountQuery=util.format(tankCountQueryString,pid);
        var tankCountResult=yield databaseUtils.executeQuery(tankCountQuery);

        var bankResult=yield databaseUtils.executeQuery(util.format('select sum(amount) as total from collection_mode cm left join shift_collection2 sc on cm.id=sc.collection_mode_id where cm.mode=\'bank\' and cm.pid=%s and date(sc.ts)=curdate()',pid));
        var cashResult=yield databaseUtils.executeQuery(util.format('select sum(amount) as total from collection_mode cm left join shift_collection2 sc on cm.id=sc.collection_mode_id where cm.mode=\'cash\' and cm.pid=%s and date(sc.ts)=curdate()',pid));
        var fleetResult=yield databaseUtils.executeQuery(util.format('select sum(amount) as total from collection_mode cm left join shift_collection2 sc on cm.id=sc.collection_mode_id where cm.mode=\'fleet\' and cm.pid=%s and date(sc.ts)=curdate()',pid));
        var creditResult=yield databaseUtils.executeQuery(util.format('select sum(amount) as total from collection_mode cm left join shift_collection2 sc on cm.id=sc.collection_mode_id where cm.mode=\'credit\' and cm.pid=%s and date(sc.ts)=curdate()',pid));
        

        yield this.render('dashboard',{
            petrolpumpResult:petrolpumpResult[0],
            saleResult:saleResult,
            quantityResult:quantityResult,
            shiftResult:shiftResult[0],
            shiftDetailsResult:shiftDetailsResult,
            nozzleCountResult: nozzleCountResult[0],
            tankCountResult:tankCountResult[0],
            bankResult:bankResult[0],
            cashResult:cashResult[0],
            fleetResult:fleetResult[0],
            creditResult:creditResult[0],
        });

    },

    logout: function* (next) {
        var sessionid = this.cookies.get("SESSION_id");
        if(sessionid) {
            sessionUtils.deleteSession(sessionid);
        }
        this.cookies.set("SESSION_id", '', {expires: new Date(1), path: '/'});

        this.redirect('/');
    },

    addNewPetrolPump: function* (next) {
        var name=this.request.body.fields.Name;
        var address=this.request.body.fields.Address;
        var city=this.request.body.fields.City;
        var pincode=this.request.body.fields.Pincode;
        var registration=this.request.body.fields.Registration;
        var mail=this.request.body.fields.mail;
        var phone=this.request.body.fields.Phone;
        var state=this.request.body.fields.State;
        var queryString='insert into petrolpump (name,address,city,pincode,registration,email,phone,state) \
        values("%s","%s","%s",%s,"%s","%s",%s,"%s")';
        var query=util.format(queryString,name,address,city,pincode,registration,mail,phone,state);
        var errormsg;
        var pid;
        var rid;
        try{
        var res=yield databaseUtils.executeQuery(query);
        pid=res.insertId;
        var fname=this.request.body.fields.fname;
        var lname=this.request.body.fields.lname;
        address=this.request.body.fields.aaddress;
        phone=this.request.body.fields.aphone;
        mail=this.request.body.fields.aemail;
        var adhaar=this.request.body.fields.adhaar;
        var password=this.request.body.fields.psw;
        var password2=this.request.body.fields.psw2;
        var filee=this.request.body.files;
        var pp;
        try{
            var pic=filee.pic.path.split('/');
            pp=pic[pic.length-1];
        }catch(e){
console.log(filee.pic.path);
            console.log('split waala kaam nahi hua');
            
        }
        console.log(password,password2);
        if(password==password2){
            console.log('password matched');
        queryString='insert into user (pid,fname,lname,address,phone,email,adhaar,password,pic)\
        values(%d,"%s","%s","%s",%s,"%s",%s,"%s","%s")';
        query=util.format(queryString,pid,fname,lname,address,phone,mail,adhaar,password,pp);
        res=yield databaseUtils.executeQuery(query);
        var uid=res.insertId;
        
            var res=yield databaseUtils.executeQuery(util.format('insert into role (type,pid) values(\'admin\',%s)',pid));
            rid=res.insertId;
        var r=yield databaseUtils.executeQuery(util.format('insert into user_role (pid,user_id,role_id)\
        values(%s,%s,%s)',pid,uid,rid));
        r=yield databaseUtils.executeQuery(util.format('insert into collection_mode (pid,mode) values(%s,"%s")',pid,'bank'));
        r=yield databaseUtils.executeQuery(util.format('insert into collection_mode (pid,mode) values(%s,"%s")',pid,'credit'));
        }
        else {
            console.log(1/0);
        }
    }

    catch(e){
      //  if(pid){
      //  var r=yield databaseUtils.executeQuery(util.format('delete from role where pid=%s',pid));
       // r=yield databaseUtils.executeQuery(util.format('delete from petrolpump where id=%s',pid));
       /// }
        errormsg=e;
        console.log('else mein gaya aur ghanta hua');
        console.log(e);
    }
        if(errormsg){
            yield this.render('addPetrolPump',{
                errormsg:errormsg,
        });
        }
        else{
            //this.redirect('/app/login');
            var msg='You have Successfully Create An Account...\nNow Login to proceed further...';
            yield this.render('login',{
                msg:msg,
            });
        }

    },

    showEmpEnteriesPage: function* (next) {
        // var pid=this.request.body.fields.pid;
        // var fname=this.request.body.fields.firstname;
        // var lname=this.request.body.fields.lastname;
        // var address=this.request.body.fields.address;
        // var phone=this.request.body.fields.phone;
        // var email=this.request.body.fields.email;

        // var adhar=this.request.body.fields.adhar;
        
        // var filee=this.request.body.files;
        // var tph=filee.pid.path.split('/');
        
        // var queryString1='INSERT INTO USER (pid,FNAME,LNAME,ADDRESS,PHONE,EMAIL,ADHAAR,pid) \
        // VALUES(%s,"%s","%s","%s",%s,"%s",%s,"%s")';
        // var query1=util.format(queryString1,pid,fname,lname,address,phone,email,adhar,tph[tph.length-1]);
        // var res1=yield databaseUtils.executeQuery(query1);
        var s=this.currentUser;
        console.log(s);
        console.log(s[0].pid);

        yield this.render('msg',{
    });
    },

    showEmpEnteries1Page: function* (next) {
        yield this.render('empent',{

    });
},

   showNewPetrolPumpPage: function* (next) {
        yield this.render('addPetrolPump',{
            errormsg:false,
    });
},

showLoginPage: function* (next) {
    var msg;
    yield this.render('login',{
        msg:msg,
});
},
showmsgpage: function* (next) {

    yield this.render('msg',{

});
},
showidash: function*(next){

    yield this.render('idash',{
        
    });
},
showHeader: function*(next){
    yield this.render('newheader',{

    });
},
showmodal:function*(next){
    yield this.render('modalsample',{});
},
showHomePage:function*(next){
    yield this.render('home',{});
},
showPage2:function*(next){
    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}
            //For total sale of HSD and MS
            var saleQueryString='select n.type,sum(dor-dcr-pump_test-self) as sale from shift s,shift_nozzle_dsm snd,nozzle n where snd.nozzle_id=n.id and snd.shift_id=s.id and date(s.start_time)=curdate() and s.pid=%s group by n.type';
            var saleQuery=util.format(saleQueryString,pid);
            var saleResult=yield databaseUtils.executeQuery(saleQuery);

            var quantityResult=yield databaseUtils.executeQuery(util.format('select type,qty from tank_metadata where pid=%s',pid));
    
            var nozzleCountQueryString='select count(*) as c from nozzle where pid=%s';
            var nozzleCountQuery=util.format(nozzleCountQueryString,pid);
            var nozzleCountResult=yield databaseUtils.executeQuery(nozzleCountQuery);

            var openReading=yield databaseUtils.executeQuery(util.format('select dor from shift_nozzle_dsm snd,shift s where pid=%s and s.id=snd.shift_id order by shift_id desc,nozzle_id limit %s',pid,nozzleCountResult[0].c));
            
            var tankCountQueryString='select count(*) as c from tank_metadata where pid="%s"';
            var tankCountQuery=util.format(tankCountQueryString,pid);
            var tankCountResult=yield databaseUtils.executeQuery(tankCountQuery);
    
            var bankResult=yield databaseUtils.executeQuery(util.format('select sum(amount) as total from collection_mode cm left join shift_collection2 sc on cm.id=sc.collection_mode_id where cm.mode=\'bank\' and cm.pid=%s and date(sc.ts)=curdate()',pid));


            var fleetResult=yield databaseUtils.executeQuery(util.format('select sum(amount) as total from collection_mode cm left join shift_collection2 sc on cm.id=sc.collection_mode_id where cm.mode=\'fleet\' and cm.pid=%s and date(sc.ts)=curdate()',pid));
            var shiftDetails=yield databaseUtils.executeQuery(util.format('select * from shift s,user u where s.pid=%s and u.id=s.supervisor_id order by s.id desc limit 1',pid));
    
            var petrolpump=yield databaseUtils.executeQuery(util.format('select * from petrolpump where id=%s',pid));

            console.log(openReading.length,'length');
            yield this.render('page2',{
                saleResult:saleResult,
                quantityResult:quantityResult,
                nozzleCountResult: nozzleCountResult[0],
                tankCountResult:tankCountResult[0],
                bankResult:bankResult[0],
                fleetResult:fleetResult[0],
                openReading:openReading,
                shiftDetails:shiftDetails,
                petrolpump:petrolpump[0],
            });

},
showshiftpage2:function*(next){
    yield this.render('shift2',{});
},
showshiftpage3:function*(next){
    yield this.render('shift3',{});
},

}


