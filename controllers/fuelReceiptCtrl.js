var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');


module.exports = {
showfuelReceipt: function*(next){
    var fuelDetails;
    if(this.currentUser){
    var fuelReceiptqueryString='select * from fuel_receipt f,transport t where f.tid=t.id and f.pid=%s';
    var fuelReceiptquery=util.format(fuelReceiptqueryString,this.currentUser.pid);
    var result=yield databaseUtils.executeQuery(fuelReceiptquery);
    fuelDetails=result;
    }
    yield this.render('fuelReceipt',{
        fuelDetails:fuelDetails
    });
},
}