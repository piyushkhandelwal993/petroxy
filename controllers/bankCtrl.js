var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
showiBankPage: function*(next){
    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}
   
    var queryString='select id,bank,account_no,balance,active from bank_account where pid=%s';
    var query=util.format(queryString,pid);
    var bankResult=yield databaseUtils.executeQuery(query);


    yield this.render('ibank',{

        bankResult:bankResult
        
    });
},


showiBank2Page: function*(next){
    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}
    var act=this.request.body.act.split(' ');

    console.log(act);


    if(parseInt(act[0])==2){
      //  var bank=this.request.body.bank;
        var account_no=this.request.body.account_no;
        var balance=this.request.body.balance;
        if(balance && account_no){
            queryString='update bank_account set account_no="%s",balance=%s where id=%s';
            query=util.format(queryString,account_no,balance,parseInt(act[1]));
            var res=yield databaseUtils.executeQuery(query);
        }
        else{
            var active=this.request.body.active;
            queryString='update bank_account set active=%s where id=%s';
            query=util.format(queryString,active,parseInt(act[1]));
            var res=yield databaseUtils.executeQuery(query);
        }

    
    }
       
    else if(parseInt(act[0])==1){

        var bank=this.request.body.bank;
        var account_no=this.request.body.account_no;
        var balance=this.request.body.balance;
       

        queryString='insert into bank_account (pid,bank,account_no,balance,active) \
        values(%s,"%s","%s",%s,%s)';
        console.log(pid,bank,account_no,balance);
        query=util.format(queryString,pid,bank,account_no,balance,1);
        var res=yield databaseUtils.executeQuery(query);
    }

    else{
        console.log(act);
        var eid=parseInt(act[2]);
        if(parseInt(act[1])==0){
        var res=yield databaseUtils.executeQuery(util.format('update bank_account set active=0 where id=%s',eid));
        }else{
            var res=yield databaseUtils.executeQuery(util.format('update bank_account set active=1 where id=%s',eid));
        }
    }


    this.redirect('ibank');
    /*
    yield this.render('iemployee',{
        employeeResult:employeeResult,
        employeeRole:employeeRole,
        tt:tt,
});*/
},

}


