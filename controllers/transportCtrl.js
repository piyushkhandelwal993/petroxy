var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils=require('./../utils/databaseUtils');

module.exports = {
showTransport: function*(next){
    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}

    var transportqueryString='select * from transport where pid=%s and del=0';
    var transportquery=util.format(transportqueryString,pid);
    var result=yield databaseUtils.executeQuery(transportquery);
    var transportDetails=result;
    yield this.render('transport',{
       transportDetails:transportDetails
    });
},
showTransport2 :function*(next){

    var pid;
    try{ pid=this.currentUser.pid;}
    catch(e){pid=0;}
    
    var name=this.request.body.name;
    var tp=this.request.body.tp;
    if(name){
        var re=yield databaseUtils.executeQuery(util.format('insert into transport (pid,name) values(%s,"%s")',pid,name));
    }
    else{
        var id=this.request.body.act;
        var re=yield databaseUtils.executeQuery(util.format('update transport set del=1 where id=%s',id));
    }
    this.redirect('/app/transport');
},
}

