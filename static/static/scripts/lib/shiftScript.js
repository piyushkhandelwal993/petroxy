 $(document).ready(function(){
           var bankid;
           var creditid;
           var creditParty;
           $.post('/api/getpmodes',{},function(data,status){
             bankid=data[0];
             creditid=data[1];
             console.log(bankid,creditid);
           });
             var submissions=-1;
             var partycount=0;
             var bankcount=-1;
              var nonfuels=-1;
              var fuelreceipts=-1;

           function normal(){
             ++submissions;
               var f1=$('<input />',{
                 type:'text',
                 value:$('#dsmUsers').val(),
                 id:'f1',
                 name:'user'+submissions,
                 class:'form-control'
               });
               //f1.prop('disabled',true);
              
               var f2=$('<input />',{
                 type:'text',
                 value:$('#paymentModes').val(),
                 id:'f2',
                 name:'mode'+submissions,
                 class:'form-control'
               });
               //f2.prop('disabled',true);

               var f3=$('<input/>',{
                 type:'text',
                 class:'form-control',
                 id:'f3',
                 name:'amount'+submissions,
                 placeholder:'Amount'
               });

             var ibtn=$('<button />',{
               class:"btn btn-danger",
               id:'dbtn'+submissions,
               html:"<h4>&times;</h4>",
               value:$('option:selected','#paymentModes').attr('data-cap')+' '+$('option:selected','#dsmUsers').attr('data-cap'),
               name:'subname'+submissions
             });
              var roww=$('<tr />',{
                id:'row'+submissions
              });
              var col1=$('<td />',{}).append(f1);
              var col2=$('<td />',{}).append(f2);
              var col3=$('<td />',{}).append(f3);
              var col4=$('<td />',{}).append(ibtn);
             roww.append(col1);
             roww.append(col2);
             roww.append(col3);
             roww.append(col4);
             ibtn.click(function(){
              --submissions;
              roww.remove();
            });
            $('#submissionTable').append(roww);
            }


            function bank(){
              ++bankcount;
              ++submissions;
              var f1=$('<input/>',{
                 type:'text',
                 value:$('#dsmUsers').val(),
                 id:'f1',
                 name:'user'+submissions,
                 class:'form-control'
               });
               //f1.prop('disabled',true);
              
               var f2=$('<input/>',{
                 type:'text',
                 value:$('#paymentModes').val(),
                 id:'f2',
                 name:'mode'+submissions,
                 class:'form-control'
               });
               //f2.prop('disabled',true);
               var f3=$('<input/>',{
                 type:'text',
                 class:'form-control',
                 id:'f3',
                 name:'amount'+submissions,
                 placeholder:'Amount'
               });
             var ibtn=$('<button/>',{
               class:"btn btn-danger",
               id:'dbtn'+submissions,
               html:"<h4>&times;</h4>",
               value:$('option:selected','#paymentModes').attr('data-cap')+' '+$('option:selected','#dsmUsers').attr('data-cap')+' 1',
               name:'subname'+submissions
             });
              var roww=$('<tr />',{
                id:'row'+submissions
              });
              var col1=$('<td />',{}).append(f1);
              var col2=$('<td />',{}).append(f2);
              var col3=$('<td />',{}).append(f3);
              var col4=$('<td />',{}).append(ibtn);
             roww.append(col1);
             roww.append(col2);
             roww.append(col3);
             roww.append(col4);
                var tf1=$('<input />',{
                  type:'text',
                  value:'bank name',
                  class:'form-control'
                });
                tf1.prop('disabled',true);
                var stf2=$('<select />',{
                  class:'form-control',
                  name:'bank'+submissions,
                });
                  var tf3=$('<input />',{
                  type:'text',
                  value:'Account No.',
                  class:'form-control'
                });
                tf3.prop('disabled',true);

                var stf4=$('<select />',{
                  class:'form-control',
                  name:'account'+submissions,
                });
                $.post('/api/getbankaccount',{},function(data,status){
                  for(i in data) {
                    stf2.append('<option value="'+data[i].bank+'">'+data[i].bank+'</option>');
                    stf4.append('<option value="'+data[i].id+'">'+data[i].account_no+'</option>');
                  }

                stf2.change(function(){
                  var bankname=$(this).val();
                  $.post('/api/getbankaccount',{bank:bankname},function(data,status){
                    stf4.find('option').remove();
                    for(i in data) stf4.append('<option value="'+data[i].id+'">'+data[i].account_no+'</option>');
                  });
                });

                });
                var tcol1=$('<td />',{});
                var tcol2=$('<td />',{});
                var tcol3=$('<td />',{});
                var tcol4=$('<td />',{});
                tcol1.append(tf1);
                tcol2.append(stf2);
                tcol3.append(tf3);
                tcol4.append(stf4);
                var troww=$('<tr />',{});
                troww.append(tcol1);
                troww.append(tcol2);
                troww.append(tcol3);
                troww.append(tcol4);
               $('#submissionTable').append(roww);
               $('#submissionTable').append(troww);

               ibtn.click(function(){
              --submissions;
                roww.remove();
                troww.remove();
            });
            }

             function party(){
              ++partycount;
              ++submissions;
              var tabl=$('<table />',{
                class:'table-responsive-md'
              });
              
              var f1=$('<input />',{
                 type:'text',
                 value:$('#dsmUsers').val(),
                 id:'f1',
                 name:'user'+submissions,
                 class:'form-control'
               });
              // f1.prop('disabled',true);
              

               var f2=$('<input />',{
                 type:'text',
                 value:$('#paymentModes').val(),
                 id:'f2',
                 name:'mode'+submissions,
                 class:'form-control'
               });
              // f2.prop('disabled',true);

               var f3=$('<button />',{
                 type:'button',
                 class:'form-control btn btn-outline-primary',
                 id:'dbtn',
                  html:'+ Party'
               });
               var ibtn=$('<button/>',{
               class:"btn btn-danger",
               id:'dbtn'+submissions,
               html:"<h4>&times;</h4>",
               value:$('option:selected','#paymentModes').attr('data-cap')+' '+$('option:selected','#dsmUsers').attr('data-cap')+' 2 1',
               name:'subname'+submissions
             });
            
            var tcol1=$('<td />',{});
            var tcol2=$('<td />',{});
            var tcol3=$('<td />',{});
            var tcol4=$('<td />',{});
            tcol1.append(f1);
            tcol2.append(f2);
            tcol3.append(f3);
            tcol4.append(ibtn);
            var thro=$('<tr />',{});
             thro.append(tcol1);
             thro.append(tcol2);
             thro.append(tcol3);
             thro.append(tcol4);
            $('#submissionTable').append(thro);
            --submissions;
            var store=[];
            store.push(creditrowfunction());
            f3.click(function(){
              var temp=ibtn.val().split(' ');
              ibtn.val(temp[0]+' '+temp[1]+' '+temp[2]+' '+(parseInt(temp[3])+1));
              store.push(creditrowfunction());
               });
             ibtn.click(function(){
              //--submissions;
              thro.remove();
              
              
              for(i in store) {store[i].remove();--submissions; 
                var temp=ibtn.val().split(' ');
              ibtn.val(temp[0]+' '+temp[1]+' '+temp[2]+' '+(parseInt(temp[3])-1));
              }
            });
            
             function creditrowfunction(){
               ++submissions;
              var tf1=$('<select />',{
                class:'form-control',
                id:'party'+submissions,
                name:'party'+submissions,
              });
              $.post('/api/getcreditparties',{},function(data,status){
                for(i in data){
                  tf1.append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
                 
                }
                // tf2.val(data[0].ADDRESS);
                //     tf3.val(data[0].PHONE);
              });
              var tf2=$('<input />',{
                type:'text',
                id:'vehicle'+submissions,
                name:'vehicle'+submissions,
                class:'form-control',
                placeholder:'Vehicle No.',
              });
              var tf3=$('<input />',{
                type:'text',
                id:'invoice'+submissions,
                name:'invoice'+submissions,
                class:'form-control',
                placeholder:'Invoice No.'
              });
              // tf1.change(function(){
              //   var partyid=$(this).val()
              //   $.post('/api/getcreditparties',{id:partyid},function(data,status){
              //     for(i in data){
              //       // tf2.val(data[i].ADDRESS);
              //       // tf3.val(data[i].PHONE);
              //     }
              //   });
              // });
              var tf4=$('<input/>',{
                 type:'text',
                 class:'form-control',
                 id:'f3',
                 name:'amount'+submissions,
                 placeholder:'Amount'
                });


              var tcol1=$('<td />',{});
              var tcol2=$('<td />',{});
              var tcol3=$('<td />',{});
              var tcol4=$('<td />',{});
              tcol1.append(tf1);
              tcol2.append(tf2);
              tcol3.append(tf3);
              tcol4.append(tf4);
              var trow=$('<tr />',{});
              trow.append(tcol1);
              trow.append(tcol2);
              trow.append(tcol3);
              trow.append(tcol4);
              $('#submissionTable').append(trow);
              return trow;
             }
            }


             $('#createSubmission').click(function(){
               //++submissions;
               var id=$('option:selected','#paymentModes').attr('data-cap');
              if(id==bankid) bank();
              else if(id==creditid) party();
              else normal();
              });
            
              $("#myBtn").click(function(){
                 $("#myModal").modal();
                 });

                 /*

            $('#act').click(function(){
              $('#confirm').val(submissions);
            });
            */
            

            $('#addfuel').click(function(){
              ++fuelreceipts;
              var div=$('<div />',{

              });
              var tab=$('<table />',{
                align:'center',
                class:'table table-hover'
              });

              var f1=$('<input />',{type:'text',name:'ttno'+fuelreceipts,class:'form-control',});
              var f2=$('<select />',{class:'form-control',name:'transport'+fuelreceipts});
              $.post('/api/gettransports',{},function(data,status){
                f2.append('<option value="0">Select Transport</option>')
                for(i in data) f2.append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
              });
              

             // var f2=$('<input />',{type:'text',name:'transport'+fuelreceipts,class:'form-control',});

              var f3=$('<select />',{class:'form-control',name:'product'+fuelreceipts});
              $.post('/api/getfuels',{},function(data,status){
                f3.append('<option value="0">Select Fuel type</option>')
                for(i in data) f3.append('<option value="'+data[i].id+'">'+data[i].type+'</option>');
              });



            //  var f3=$('<input />',{type:'text',name:'product'+fuelreceipts,class:'form-control',});





              var f4=$('<input />',{type:'text',name:'invoiceno'+fuelreceipts,class:'form-control',});
              var f5=$('<input />',{type:'date',name:'invoicedate'+fuelreceipts,class:'form-control',});
              var f6=$('<input />',{type:'text',name:'invoiceamount'+fuelreceipts,class:'form-control',});
              var f7=$('<input />',{type:'text',name:'invoiceqty'+fuelreceipts,class:'form-control',});
              var f8=$('<input />',{type:'text',name:'invoicetemp'+fuelreceipts,class:'form-control',});
              var f9=$('<input />',{type:'text',name:'rotemp'+fuelreceipts,class:'form-control',});
              var f10=$('<input />',{type:'text',name:'invoicedensity'+fuelreceipts,class:'form-control',});
              var f11=$('<input />',{type:'text',name:'rodensity'+fuelreceipts,class:'form-control',});
              var f12=$('<input />',{type:'text',name:'tva'+fuelreceipts,class:'form-control',});
              var f13=$('<input />',{type:'text',name:'short'+fuelreceipts,class:'form-control',});
              var f14=$('<input />',{type:'text',name:'finvoiceno'+fuelreceipts,class:'form-control',});
              var f15=$('<input />',{type:'text',name:'finvoiceamount'+fuelreceipts,class:'form-control',});

              var tbheader=$('<th />',{
                colspan:2,
                html:'Fuel Receipt '+fuelreceipts
              });

              var tbheader1=$('<th />',{
                html:'Fuel Receipt '+fuelreceipts,
                colspan:2,
                class:'dark-primary-color text-light',
              });

              var t=$('<input />',{
                type:'button',
                class:'btn btn-danger float-right',
                value:'X',
              });
              tbheader1.append(t);
              t.click(function(){
                --fuelreceipts;
                $(this).parent().parent().remove();
              });

              tab.append(tbheader1);
              var col1;var col2;var roww;

              col1=$('<td />',{html:'TT No.'});
              col2=$('<td />',{}).append(f1);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Transport name'});
              col2=$('<td />',{}).append(f2);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Product name'});
              col2=$('<td />',{}).append(f3);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Invoice No.'});
              col2=$('<td />',{}).append(f4);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Invoice date'});
              col2=$('<td />',{}).append(f5);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Invoice Amount'});
              col2=$('<td />',{}).append(f6);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Invoice Quantity'});
              col2=$('<td />',{}).append(f7);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Invoice Temp'});
              col2=$('<td />',{}).append(f8);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'RO Temp'});
              col2=$('<td />',{}).append(f9);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Invoice Density'});
              col2=$('<td />',{}).append(f10);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'RO Density'});
              col2=$('<td />',{}).append(f11);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'TVA'});
              col2=$('<td />',{}).append(f12);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Shortage Booked'});
              col2=$('<td />',{}).append(f13);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Freight Invoice No.'});
              col2=$('<td />',{}).append(f14);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              col1=$('<td />',{html:'Freight Inv. Amount'});
              col2=$('<td />',{}).append(f15);
              roww=$('<tr />',{});roww.append(col1);roww.append(col2);
              tab.append(roww);

              
              div.append(tab);
              $('.fuelCame').append(div);

              


            });


            $('#act').click(function(){
              $('#confirm').val(submissions);
              var myval=$(this).val().split(' ');
              $(this).val(myval[0]+' '+myval[1]+' '+myval[2]+' '+myval[3]+' '+submissions+' '+fuelreceipts+' '+nonfuels);
              $('#hiddenVal').val($('#act').val());
              $('#endshiftform').submit();
              $(this).attr('type','submit');
              console.log('trying to submit...');
            });

            // $('#addfuel').click(function(){
            //   addnonfuel();
            // });            





            $('#addnonfuel').click(function(){
              var trow=$('<tr />',{});
              var tcol1=$('<td />',{});
              var tcol2=$('<td />',{});
              var tcol3=$('<td />',{});
              var tcol4=$('<td />',{});
              //yahaan add kar raha hu
              var tcol35=$('<td />',{});
              
              ++nonfuels;


              // var f1=$('<select />',{
              //   class:'form-control',
              //   id:'meradsm'+nonfuels,
              //   name:'meradsm'+nonfuels,
              // });

              var f1=$('<select />',{
                class:'form-control',
                id:'nonfuel'+nonfuels,
                name:'meradsm'+nonfuels,
              });



              $.post('/api/getdsms',{},function(data,status){
                console.log(data,status);
                f1.append('<option value="'+'0'+'">'+'Select DSM'+'</option>');
                for(i in data) f1.append('<option value="'+data[i].id+'">'+data[i].fname+ ' '+data[i].lname+'</option>');
              });



              var f2=$('<select />',{
                class:'form-control',
                id:'nonfuel'+nonfuels,
                name:'nonfuelitem'+nonfuels,
              });

              $.post('/api/getproducts',{},function(data,status){
                console.log(data,status);
                f2.append('<option value="0">Select Product</option>');
                for(i in data) f2.append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
              });

              var f3=$('<input />',{
                type:'text',
                id:'nfamount'+nonfuels,
                name:'nonfuelqty'+nonfuels,
                class:'form-control',
              });
              f3.prop('required',true);

              var f35=$('<input />',{
                type:'text',
                id:'nfamount',
                class:'form-control',
              });

              var f4=$('<button/>',{
              class:"btn btn-default",
              id:'dbtn'+submissions,
              html:"<h4>&times;</h4>",
              value:'2 1',
              name:'nfamount'+nonfuels
              });

              tcol1.append(f1);
              tcol2.append(f2);
              tcol3.append(f3);
              tcol35.append(f35); //yahaan add kiya
              tcol4.append(f4);
              trow.append(tcol1);
              trow.append(tcol2);
              trow.append(tcol35);
              trow.append(tcol3);
              trow.append(tcol4);
              f1.change(function(){
                f4.val(f1.val()+' '+f2.val());
              });
              f2.change(function(){
                f4.val(f1.val()+' '+f2.val());
              });

              $('#nonfueltable').append(trow);
              f4.click(function(){
                trow.remove();
              });

              f35.on('keydown',function(){
                console.log('laga re laga');
                $.post('/api/getproductprice',{product:f2.val()},function(data,status){
                  f3.val(parseInt(f35.val())*data.price);
                  if(f3.val()=='NaN') f3.val(0);
                });
              });

              });



          $('.shift').click(function(){
            window.location=$(this).find('a').attr('href');
          }).hover(function(){
            $(this).toggleClass('hover');
          });
        });