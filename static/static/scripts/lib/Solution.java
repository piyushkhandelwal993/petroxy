class Solution{
    public int solution(int [] A) {
        int maxSize=1;
        int maxSizeSoFar=1;
        int ind=0;
        int curele=A[0];
        for(int i=1;i<A.length;++i){
            if(A[i]>curele) maxSize++;
            else{
                curele=A[i];
                maxSizeSoFar=max(maxSizeSoFar,maxSize);
                maxSize=1;
            }
        }
        maxSizeSoFar=max(maxSizeSoFar,maxSize);
        return maxSizeSoFar;
    }
    public static int max(int a,int b){
        if(a>b) return a;
        return b;
    }
    public static void main(String[] args) {
      Solution s;
      int A[]={-2,2,2,2,1,2,-1,2,1,3};
      System.out.println(s.solution(A));  
    } 
}