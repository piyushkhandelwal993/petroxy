$(document).ready(function(){

    function getState(){
        $.get('/api/state',{},function(data,status){
            for(i in data) $('#state').append('<option value="'+data[i]+'">'+data[i]+'</option>');
        });
    }

    getState();
    $('#state').change(function(){
        var stateval=$('#state').val();
        $('#city option:not(:first)').remove();
        if(stateval!="0"){
            $.get('/api/district',{state:stateval},function(data,status){
                
                for(i in data) $('#city').append('<option value="'+data[i]+'">'+data[i]+'</option>');
            });
        }
    });
    $('#pemail').focusout(function(){
        $.post('/api/checkpmail',{email:$(this).val()},function(data,status){
            console.log(data);
            if(data.flag=='1') $('#pemail').css({"color":"green"});
            else {
                $('#pemail').css({"color":"red"});
            }
        });
    });

    $('#pphone').focusout(function(){
        $.post('/api/checkpphone',{phone:$(this).val()},function(data,status){
            console.log(data,status);
            if(data.flag=='1') $('#pphone').css({"color":"green"});
            else $('#pphone').css({"color":"red"});
        });
    });

    $('#aemail').focusout(function(){
        $.post('/api/checkaemail',{email:$(this).val()},function(data,status){
            console.log(data,status);
            if(data.flag=='1') $('#aemail').css({"color":"green"});
            else $('#aemail').css({"color":"red"});
        });
    });

    $('#aphone').focusout(function(){
        $.post('/api/checkaphone',{phone:$(this).val()},function(data,status){
            console.log(data,status);
            if(data.flag=='1') $('#aphone').css({"color":"green"});
            else $('#aphone').css({"color":"red"});
        });
    });

    $('#adhaar').focusout(function(){
        $.post('/api/checkadhaar',{adhaar:$(this).val()},function(data,status){
            console.log(data,status);
            if(data.flag=='1') $('#adhaar').css({"color":"green"});
            else $('#adhaar').css({"color":"red"});
        });
    });
});