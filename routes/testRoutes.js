var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

     var testCtrl = require('./../controllers/testCtrl');
     router.get('/state', testCtrl.showdistrict);
     router.get('/district', testCtrl.getdistricts);
     router.post('/getpmodes',testCtrl.getmodes);
     router.post('/getcreditparties',testCtrl.getcreditparty);
    router.post('/getbankaccount',testCtrl.getbankaccount);
    router.post('/getproducts',testCtrl.getproducts);
    router.post('/getdsms',testCtrl.getdsms);
    router.post('/getfuels',testCtrl.getfuels);
    router.post('/gettransports',testCtrl.gettransports);
    router.post('/getfuelprice',testCtrl.getfuelprice);
    router.post('/setfuelprices',testCtrl.setfuelprices);
    router.post('/checkpmail',testCtrl.checkpmail);
     router.post('/checkpphone',testCtrl.checkpphone);
     router.post('/checkaemail',testCtrl.checkaemail);
     router.post('/checkadhaar',testCtrl.checkadhaar);
     router.post('/checkaphone',testCtrl.checkaphone);
     router.post('/getnooftanks',testCtrl.getnooftanks);
     router.post('/getproductprice',testCtrl.getproductprice);
     router.post('/getmodes1',testCtrl.getmodes1);

    return router.middleware();
}